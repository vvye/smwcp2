header
lorom
!GFXFile1 = #$0606
!GFXFile2 = #$0607
!GFXFile3 = #$0608
!GFXFile4 = #$0609
!NumGFXFiles = #$03
!DecompressionBuffer = $7E2000
!FileLength = #$3800

;org $80FFD8					; 128KB SRAM
;	db $07

org $809391
autoclean jml UploadGFX

org $80821D
    jml FixMarioUpload

org $80A300
    FixMarioUploadReturn1:

org $80823D
    FixMarioUploadReturn2:

org $8093F4
UploadGFXReturn:

freecode
prot Palette
reset bytes
UploadGFX:
    phb
    phk
    plb
    rep #$10
    ldx #$0000
    sep #$10
    lda #$80
    STa $2115       ;Set increment + no remapping for the Video Port Control
    STZ $2116       ;VRAM Address Lo
    STZ $2117       ;VRAM Address Hi
    ldx !NumGFXFiles
    .UploadLoop
        phx
        txa
        asl
        tax
        LDA.b #!DecompressionBuffer>>16
        sta $02
        rep #$20
        lda.w #!DecompressionBuffer
        sta $00
        lda GFXFiles,x
        JSL $8FF900
        sep #$20
        plx

        rep #$10
        LDy #$1801      ;43x0 - 1 registers write once
        STy $4340       ;43x1 - $2118 - VRAM Data Write lo byte
        LDy.w #!DecompressionBuffer    ;Mode 7 Tilemap Offset (Inserted with xkas)
        STy $4342       ;DMA Source Address Offset
        LDA.b #!DecompressionBuffer>>16    ; Mode 7 Tilemap Bank
        STA $4344       ;DMA Source Address Bank
        LDy !FileLength      ;How many bytes to write
        STy $4345       ;DMA Size hi/lo byte
        LDA #$10        ;Free DMA channel
        STA $420B       ;Enable DMA
        sep #$10

        dex
        bpl .UploadLoop

        rep #$30
        lda #$0000
        ldx #$0000
        .Unroll
        sta !DecompressionBuffer,x
        inx
        inx
        inc
        cmp #$0400
        bcc .Unroll
        sep #$20

        ;rep #$10
        LDy #$1801      ;43x0 - 1 registers write once
        STy $4340       ;43x1 - $2118 - VRAM Data Write lo byte
        LDy.w #!DecompressionBuffer    ;Mode 7 Tilemap Offset (Inserted with xkas)
        STy $4342       ;DMA Source Address Offset
        LDA.b #!DecompressionBuffer>>16    ; Mode 7 Tilemap Bank
        STA $4344       ;DMA Source Address Bank
        LDy #$1000      ;How many bytes to write
        STy $4345       ;DMA Size hi/lo byte
        LDA #$10        ;Free DMA channel
        STA $420B       ;Enable DMA
        sep #$10

        rep #$20
        ldy.b #$00
        sty.w $2121
        lda.w #$2200
        sta.w $4320
        lda.w #Palette&0xFFFF
        ldy.b $00
        sta.w $4322
        ldy.b #(Palette>>16)&0x7F
        sty.w $4324
        lda.w #$0200
        sta.w $4325
        ldx.b #$04
        stx.w $420B
        sep #$20

    lda.b #(0x7000>>10<<2)
    sta $002107
    stz $210B
    lda #$01
    sta $212C
    stz $212D
    stz $212E
    stz $212F
    stz $2133
    stz $192E
    stz $1B92
    stz $1A
    stz $210D
    lda #$FF
    sta $1C
    sta $210E
    lda #$03
    sta $2105
    sta $3E

    LDA.b #%00100000
    STA.w $2131
    STA.b $40

    LDA.b #$12
    STA.w $1DFB
    LDA.b #$80  ;Intro Length. Was #$40
    LDA.b #$0F
    STA.w $0DAE
    LDA.b #$01
    STA.w $0DAF

    lda #$01
    sta $0D9B

    STZ.w $2131
    STZ.b $40

    LDA.b #$FF
    STA.l $7FC00A

    plb
    
    STA $666667

    jml UploadGFXReturn

    FixMarioUpload: 
        LDA.w $0100
        CMP #$03
        BCC +
        PEA $823C
        JML FixMarioUploadReturn1
+       JML FixMarioUploadReturn2

    GFXFiles:
        dw !GFXFile4,!GFXFile3,!GFXFile2,!GFXFile1


freedata
Palette:
incbin presents.mw3  ;was mw3