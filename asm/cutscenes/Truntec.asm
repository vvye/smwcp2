!truntecState = !actorVar1,x

	
SetSpeakerToTruntec:
	LDX !truntecIndex
	STX !currentSpeaker
	RTL
	
TruntecSetToScannerMoving:
	LDX !truntecIndex
	LDA #$01
	STA !truntecState
	STZ !truntecStateTime
	RTL
	
TruntecSetToScanning:
	LDX !truntecIndex
	LDA #$02
	STA !truntecState
	STZ !truntecStateTime
	RTL
	
TruntecSetToScannerReturning:
	LDX !truntecIndex
	LDA #$03
	STA !truntecState
	RTL
	
TruntecSetEyeState:
	STA !truntecEyeMode
	STZ !truntecEyeIndex
	RTL

	
TruntecCreate:
	REP #$20
	LDA.w #TruntecINIT
	STA $00
	LDX.b #TruntecINIT>>16
	STX $02
	
	LDA.w #TruntecMAIN
	STA $03
	LDX.b #TruntecMAIN>>16
	STX $05
	
	LDA.w #TruntecNMI
	STA $06
	LDX.b #TruntecNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !truntecIndex
	RTL

TruntecINIT:
	STX !truntecIndex
	RTL

TruntecMAIN:
	PHK
	PLB
	PHX
	

	PHX				; Jump to the Doc's current routine.
	LDA !truntecState		;
	ASL
	TAX
	LDA TruntecStateTable,x		;
	STA $00				;
	LDA TruntecStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	JSR TruntecDrawEye
	PLX
	RTL

TruntecNMI:
	RTL
	

TruntecStateTable:
dw TruntecNormal, TruntecScannerMoving, TruntecScannerScanning, TruntecScannerReturning

TruntecTalkingXTable:
db $00, $01, $02, $03, $05, $06, $07, $08, $08, $09, $0A, $0B, $0B, $0B, $0C, $0C 
db $0C, $0C, $0C, $0B, $0B, $0B, $0A, $09, $08, $08, $07, $06, $05, $03, $02, $01 
db $00, $FF, $FE, $FD, $FB, $FA, $F9, $F8, $F8, $F7, $F6, $F5, $F5, $F5, $F4, $F4 
db $F4, $F4, $F4, $F5, $F5, $F5, $F6, $F7, $F8, $F8, $F9, $FA, $FB, $FD, $FE, $FF 

TruntecNormal:
	LDX !currentSpeaker
	CPX !truntecIndex
	BNE .notSpeaking

	LDA $13
	STA $07

	LDY #$00
.loop
	CPY #$05
	BEQ .exitLoop
	CPY #$00
	BEQ .alwaysDraw
	STY $06
	LDA $07
	CLC
	ADC $06
	AND #$01
	BNE .doNotDraw
.alwaysDraw
	TYA
	ASL
	ASL
	STA $06
	
	LDA $07
	SEC
	SBC $06
	AND #$3F
	;LSR
	TAX
	LDA TruntecTalkingXTable,x
	CLC
	ADC #$98
	STA $00
	
	CPY #$00
	BNE +
	STA !truntecLightStartX
+
	
	LDA #$40
	STA $01
	
	LDA $06
	LSR
	STA $02
	
	LDA #$32
	STA $03
	LDA #$02
	JSL DrawTile
.doNotDraw
	INY
	BRA .loop

.exitLoop
	
.notSpeaking
	RTS

;!truntecLight1StartX = $98
;!truntecLight2StartX = $98

!truntecLight1StartY = $40
!truntecLight2StartY = $40

!truntecLight1EndX = $45
!truntecLight2EndX = $5D

!truntecLight1EndY = $42
!truntecLight2EndY = $42

!truntecLight1XTravelDistance = !truntecLightStartX-!truntecLight1EndX
!truntecLight2XTravelDistance = !truntecLightStartX-!truntecLight2EndX

if !truntecLight1EndY > !truntecLight1StartY
!truntecLight1YTravelDistance = !truntecLight1EndY-!truntecLight1StartY
!truntecLight2YTravelDistance = !truntecLight2EndY-!truntecLight2StartY
else
!truntecLight1YTravelDistance = !truntecLight1StartY-!truntecLight1EndY
!truntecLight2YTravelDistance = !truntecLight2StartY-!truntecLight2EndY
endif

!truntecLightMovementLength = $29	; How many frames this animation lasts for

TruntecScannerMoving:
	LDA !truntecStateTime
	CMP #!truntecLightMovementLength+1
	BCC +
	DEC !truntecStateTime
	JMP TruntecDrawScannerLights
+
	;LDA !truntecStateTime
	;STA $4204
	;STZ $4205
	
	; Update light 1 x.  Formula: x = (timespent * destinationx)/(traveltime)
	LDA !truntecStateTime
	STA $4202
	;LDA #!truntecLight1XTravelDistance
	LDA !truntecLightStartX
	SEC
	SBC #!truntecLight1EndX
	STA $4203
	NOP
	NOP
	NOP
	REP #$20
	LDA $4216
	STA $4204
	SEP #$20
	LDA #!truntecLightMovementLength
	STA $4206
	NOP #8
	LDA !truntecLightStartX
	SEC
	SBC $4214
	STA !truntecLight1XPos
	
	; Update light 2 x
	LDA !truntecStateTime
	STA $4202
	;LDA #!truntecLight2XTravelDistance
	LDA !truntecLightStartX
	SEC
	SBC #!truntecLight2EndX
	STA $4203
	NOP
	NOP
	NOP
	REP #$20
	LDA $4216
	STA $4204
	SEP #$20
	LDA #!truntecLightMovementLength
	STA $4206
	NOP #8
	LDA !truntecLightStartX
	SEC
	SBC $4214
	STA !truntecLight2XPos
	
	; Update light 1 y.  Formula: y = (timespent * destinationy)/(traveltime)
	LDA !truntecStateTime
	STA $4202
	LDA #!truntecLight1YTravelDistance
	STA $4203
	NOP
	NOP
	NOP
	REP #$20
	LDA $4216
	STA $4204
	SEP #$20
	LDA #!truntecLightMovementLength
	STA $4206
	NOP #8
if !truntecLight1EndY > !truntecLight1StartY
	LDA $4214
	CLC
	ADC #!truntecLight1StartY
else
	LDA #!truntecLight1StartY
	SEC
	SBC $4214
	STA !truntecLight1YPos
	
endif
	STA !truntecLight1YPos
	
	; Update light 2 y
	LDA !truntecStateTime
	STA $4202
	LDA #!truntecLight2YTravelDistance
	STA $4203
	NOP
	NOP
	NOP
	REP #$20
	LDA $4216
	STA $4204
	SEP #$20
	LDA #!truntecLightMovementLength
	STA $4206
	NOP #8
if !truntecLight1EndY > !truntecLight1StartY
	LDA $4214
	CLC
	ADC #!truntecLight2StartY
else
	LDA #!truntecLight2StartY
	SEC
	SBC $4214
	STA !truntecLight2YPos
	
endif
	STA !truntecLight2YPos

	
	
	
	
	
TruntecDrawScannerLights:	
	
	; Draw light tile 1
	LDA !truntecLight1XPos
	STA $00
	LDA !truntecLight1YPos
	STA $01
	LDA #$00
	STA $02
	LDA #$32
	STA $03
	
	LDA #$02
	JSL DrawTile

	; Draw light tile 2
	LDA !truntecLight2XPos
	STA $00
	LDA !truntecLight2YPos
	STA $01
	LDA #$00
	STA $02
	LDA #$32
	STA $03
	
	LDA #$02
	JSL DrawTile
	
	INC !truntecStateTime
	
	RTS
	
TruntecScannerScanning:
	LDA !truntecStateTime
	CMP #$20
	BCC .moveDown
	CMP #$40
	BCC .hold
	CMP #$60
	BCC .moveUp
	CMP #$80
	BCC .hold
	STZ !truntecStateTime
	;RTS
	
.moveDown
	AND #$1F
.nowJustAdd
	CLC
	ADC #!truntecLight1EndY
	STA !truntecLight1YPos
	STA !truntecLight2YPos
.hold
	JSR TruntecDrawScannerLights
	LDA !truntecLight1XPos
	CLC
	ADC #$0C
	STA $00
	LDA !truntecLight1YPos
	STA $01
	LDA $13
	AND #$01
	ASL
	CLC
	ADC #$0A
	STA $02
	
	LDA #$02
	JSL DrawTile
	RTS
	
.moveUp
	LDA #$60
	SEC
	SBC !truntecStateTime
	;AND #$1F
	BRA .nowJustAdd
	
	
TruntecScannerReturning:
	
	LDA !truntecStartMovingLightsBack
	BNE ++
	LDA !truntecLight1YPos
	CMP #!truntecLight1EndY
	BEQ +
	JSR TruntecScannerScanning 
	RTS
+
	LDA #$01
	STA !truntecStartMovingLightsBack
	STA !truntecStateTime
++
	LDA !truntecStateTime
	PHA
	
	LDA #!truntecLightMovementLength
	SEC
	SBC !truntecStateTime
	STA !truntecStateTime
	
	LDA $13
	AND #$3F
	TAX
	LDA TruntecTalkingXTable,x
	CLC
	ADC #$98
	STA !truntecLightStartX
	JSR TruntecScannerMoving
	
	PLA
	STA !truntecStateTime
	CMP #!truntecLightMovementLength
	BNE +
	
	LDX !truntecIndex
	LDA #$00
	STA !truntecState
	;STA $13
	
	
+
	INC !truntecStateTime
	RTS

	
	
TruntecDrawEye:
	STZ $00
	LDA !truntecEyeMode
	CMP #$00
	BEQ .normalEye
	CMP #$01
	BEQ .hourglassEye

	LDA $13
	AND #$01
	DEC
	STA $00	
	LDA #$0F
	STA $02
	BRA .continue
	
.normalEye
	LDA #$0E
	STA $02
	BRA .continue
	
.hourglassEye
	LDA !truntecEyeIndex
	CMP #$0A
	BCC +
	LDA #$00
	STA !truntecEyeIndex
+
	CLC
	ADC #$20
	STA $02
	LDA $13
	AND #$07
	BNE +
	INC !truntecEyeIndex
	
+
	
.continue
	LDA $00
	CLC
	ADC #$9C
	STA $00
	LDA #$23
	STA $01
	LDA #$32
	STA $03
	
	LDA #$00
	JSL DrawTile
	RTS
	
	
	
	
	
	
	
	
	