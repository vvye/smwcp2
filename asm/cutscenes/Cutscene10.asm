!DeedleBallSpriteCHR = 	$0A25

Cutscene10Palette:
incbin Cutscene10.rawpal
	
CSINIT10:
	STZ $2121
	%StandardDMA(#$2122, Cutscene10Palette, #512, #$00) 
	
	REP #$30	
	LDA #!DeedleBallSpriteCHR		; |
	LDX #$C000>>1				; | Upload Deedle Ball
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	
	SEP #$30
	
	LDA #$05
	STA !backgroundColor
	LDA #$06
	STA !backgroundColor+1
	LDA #$07
	STA !backgroundColor+2
	
	
	
	LDA #$00
	STA !currentFont
	
	JSL MarioCreate
	JSL NorvegCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$53 : JSL MarioSetTargetX

	
	RTL

	

CS10T1:
;db $F4
;db "And there's the drink you ordered, Your Burliness.", $EF, $EB
;%VWFASM(SetSpeakerToBowser)
;db "I gotta say, the Doc was right about this whole vacation thing! Those constant battles with Mario really take a lot out on a guy!", $EF, $EB
;%VWFASM(SetSpeakerToKamek)
;db "Someone as magnificently evil such as yourself definitely deserves a break every once in a while.", $EF, $EB
;%VWFASM(SetSpeakerToBowser)
;db "And this view--", $EE, $F6, "couldn't be better! I should totally build an awesome new resort castle here! ", $EF, $EB
;%VWFASM(SetSpeakerToKamek)
;db "Er, sire?", $EB
;%VWFASM(SetSpeakerToBowser)
;db "Gwa ha ha! It'd be perfect! Vacations whenever I want_", $EF, $EB
;%VWFASM(SetSpeakerToKamek)
;db "Excuse me, sire?", $EB
;%VWFASM(SetSpeakerToBowser)
;db "No bumbling servents getting in the way_", $EF, $EB
;%VWFASM(SetSpeakerToKamek)
;db "Sire, you might want to know that--", $EB
;%VWFASM(SetSpeakerToBowser)
;db "And best of all, no annoying interruptions from--", $EE
;%VWFASM(MarioSetToVarWalkRight)
;db $F8
;db "MARIO!"
;db $F4
db $FF, $FF, $FF, $FF, $FF, $E0

