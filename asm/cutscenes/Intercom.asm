
!intercomX = $33
!intercomY = $10

SetSpeakerToIntercom:
	LDX !intercomIndex
	STX !currentSpeaker
	RTL


IntercomCreate:
	REP #$20
	LDA.w #IntercomINIT
	STA $00
	LDX.b #IntercomINIT>>16
	STX $02
	
	LDA.w #IntercomMAIN
	STA $03
	LDX.b #IntercomMAIN>>16
	STX $05
	
	LDA.w #IntercomNMI
	STA $06
	LDX.b #IntercomNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !intercomIndex
	RTL

IntercomINIT:
	RTL

IntercomMAIN:
	LDA !textIsAppearing
	BEQ .noAnimation
	
	LDA $13
	AND #$04
.noAnimation
	STA $0C

	LDA #!intercomX :		   STA $00
	LDA #!intercomY :		   STA $01
	LDA #$48 : CLC : ADC $0C : STA $02
	LDA #$3D : STA $03
	LDA #$02 : JSL DrawTile
	
	
	LDA #!intercomX : CLC : ADC #$10 : STA $00
	LDA #!intercomY :                  STA $01
	LDA #$4A : CLC : ADC $0C : STA $02
	LDA #$02 : JSL DrawTile
	
	
	LDA #!intercomX :                  STA $00
	LDA #!intercomY : CLC : ADC #$10 : STA $01
	LDA #$68 : CLC : ADC $0C : STA $02
	LDA #$02 : JSL DrawTile
	
	
	LDA #!intercomX : CLC : ADC #$10 : STA $00
	LDA #!intercomY : CLC : ADC #$10 : STA $01
	LDA #$6A : CLC : ADC $0C : STA $02
	LDA #$02 : JSL DrawTile	
	

	RTL


IntercomNMI:
	RTL
	
