!DryadCHR1 = $0400
!DryadCHR2 = $0401


!Cutscene4BackgroundCHR1 = $0167
!Cutscene4BackgroundCHR2 = $0A2C
!Cutscene4ForegroundTilemap = $0A2D
!Cutscene4BackgroundTilemap = $0A2E

Cutscene4Palette:
incbin Cutscene4.rawpal
	
CSINIT4:
	STZ $2121
	%StandardDMA(#$2122, Cutscene4Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30
	LDA #!DryadCHR1				; \
	LDX #$C000>>1				; | Upload SP1
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!DryadCHR2				; \
	LDX #$D000>>1				; | Upload SP2
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene4BackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene4BackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene4ForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene4BackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	;LDA #$0058
	;STA $1C
	;LDA #$FFE0
	;STA $1A
	
	
	SEP #$30
	
	LDA #$03			; \
	STA !backgroundColor		; |
	LDA #$02			; |
	STA !backgroundColor+1		; |
	LDA #$01			; |
	STA !backgroundColor+2		; /
	
	REP #$20
	LDA #$00E8				; \ Layer 1 X
	STA $1A					; /
	LDA #$0050				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0000				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	
	LDA #$20
	STA $20
	STZ $21
	
	LDA #$00
	STA !currentFont
	
	JSL DryadCreate
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$78 : JSL MarioSetTargetX
	;JSL MarioSetWaitStateToNothing 
	;JSL MarioSetToVarWalkRight
	
	LDA !dryadIndex
	STA !currentSpeaker
	
	RTL


CS4T1:
db $FF
%VWFASM(SetSpeakerToDryad)
db $EA, $00
db "Munch, munch, munch.  Hee-hee!  Aren't you just a little munchy-", $EE, "munch, my munchety munchety munch-", $EE, "bunch! ", $EC, $EC, $EF
%VWFASM(DryadSetToFrozen)
db "Munch.", $EF
%VWFASM(DryadSetToNormal)
db $EB 
%VWFASM(MarioSetToVarWalkRight)
db $54, "Munchin'", $F9, $EC  
db $54, "Munchin'", $F9, $EC 
db $54, "Munchin' on the river--", $F9, $EE
%VWFASM(DryadSetToFaceMario)
db "hey, what are you doing here? You don't look very munchy at all.", $EF, $EB
%VWFASM(DryadSetToSpinning)
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $F8
%VWFASM(DryadSetToFaceMario)
db "Toxic fluid is leaking out of this fortress into the forest? Yes, ", $EA, $02, "I "
%VWFASM(DryadSetToSpinning)
db "KNOW", $EA, $00
%VWFASM(DryadSetToFaceMario)
db ". I told Norveg I'd keep it out of his precious logging areas until they've been harvested. " 
%VWFASM(DryadSetToSpinning)
db "Tell him to stop micromanaging me!"
%VWFASM(MarioSetToConfused)

db $FF, $F8, $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
%VWFASM(MarioRemoveAboveHeadSymbol)
%VWFASM(DryadSetToFaceMario)
db $FF, $FF
db "What? Of "
%VWFASM(DryadSetToSpinning)
db $E7, $02, $EA, $02
db "course" 
%VWFASM(DryadSetToFaceMario)
db $E7, $00, $EA, $00
db " I want to destroy the forest! Disgusting things, trees--", $EE, "nasty ugly brown posts, and dumb as a block of wood, at that." 
%VWFASM(DryadSetToSpinning)
db " Not like my dear little Munchers and Piranha Plants, the smarties, hee-hee! ", $EF
%VWFASM(DryadSetToFaceMario)
db "And so resilient--", $EE, "they can grow anywhere; the toxic runoff here doesn't affect them at all! ", $EF, $EB
db "Tell me, who "
db $E7, $02, $EA, $02
db "really" 
db $E7, $00, $EA, $00
db " deserves to dominate this landscape, my hale and hardy little munchy geniuses,"
%VWFASM(DryadSetToSpinning)
db " or those gormless, barky utility poles?"
db $F8
%VWFASM(DryadSetToFaceMario)
db $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(DryadSetToSpinning)
%VWFASM(DryadSetToStationary)
%VWFASM(MarioSetWaitStateToNothing)
;db $FB
;%VWFASM(MarioSetToAngry)
db $FF, $FA
%VWFASM(DryadSetToMad)
db "What? The nerve! Who are you, anyway? You're clearly not with Norveg, which is the only reason any non-", $EE, "munchy being has for being in my fortress in the first place. And you yourself are most certainly not munchy."
db $EF, $EB
%VWFASM(DryadSetToConfident)
db "And in this munch-", $EE, "or-", $EE, "be-", $EE, "munched world,"
%VWFASM(MarioSetToConfused)
db " if you're not munchy, you're the munch-ee. So why am I talking to pet food, anyway? That's something only a crazy person would do. Ah, but my dearies do like their meat tenderized a bit_"
%VWFASM(MarioAddExclamationMark)
%VWFASM(MarioSetPoseToStanding)
db $FF, $FF, $EF, $FF, $E0