
!barrelXPos = $E4,x
!barrelYPos = $D8,x
!barrelYSpeed = $AA,x
!barrelXSpeed = $B6,x

!barrelFallThroughGround = !actorVar1,x		; If set, the barrel falls through the ground
!barrelWhich = !actorVar2,x			; 0 = left barrel, 1 = right barrel
!barrelDontMove = !actorVar3,x			; If set, the barrel doesn't move.
!barrelSwapDirection = !actorVar4,x		; 0 = don't move, 1 = move right, 2 = move left
!barrelSwapTimer = !actorVar5,x			; Timer for swapping.  Used to index the sine table.

BarrelCreate:
	REP #$20
	LDA.w #BarrelINIT
	STA $00
	LDX.b #BarrelINIT>>16
	STX $02
	
	LDA.w #BarrelMAIN
	STA $03
	LDX.b #BarrelMAIN>>16
	STX $05
	
	LDA.w #BarrelNMI
	STA $06
	LDX.b #BarrelNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	LDA !barrelCount
	BNE .secondBarrel
	
	STX !barrel1Index
	LDA #$60
	STA !barrelXPos
	BRA +
.secondBarrel
	STX !barrel2Index
	LDA #$80
	STA !barrelXPos
	+
	STA !barrelWhich
	

	
	INC !barrelCount
	RTL

BarrelDeleteFirst:
	LDX !barrel1Index
	JSL DeleteActor
	RTL

BarrelDeleteSecond:
	LDX !barrel2Index
	JSL DeleteActor
	RTL

BarrelINIT:	
	LDA #$E6
	STA !barrelYPos
	LDA #$FF
	STA $14D4,x
	LDA #$01
	STA !barrelDontMove
	RTL
	
BarrelFirstFall:
	LDX !barrel1Index
	BRA MakeBarrelFall
BarrelSecondFall:
	LDX !barrel2Index
MakeBarrelFall:
	LDA #$00
	STA !barrelDontMove
	RTL
	
BarrelFirstFallThroughGround:
	LDX !barrel1Index
	BRA MakeBarrelFallThroughGround
BarrelSecondFallThroughGround:
	LDX !barrel2Index
MakeBarrelFallThroughGround:
	LDA #$01
	STA !barrelFallThroughGround
	RTL

Barrel1SwapRight:
	LDX !barrel1Index
	LDA #$00
	STA !barrelSwapTimer
	LDA #$01
	STA !barrelSwapDirection
	RTL
	
Barrel1SwapLeft:
	LDX !barrel1Index
	LDA #$00
	STA !barrelSwapTimer
	LDA #$02
	STA !barrelSwapDirection
	RTL

Barrel2SwapRight:
	LDX !barrel2Index
	LDA #$00
	STA !barrelSwapTimer
	LDA #$01
	STA !barrelSwapDirection
	RTL

Barrel2SwapLeft:
	LDX !barrel2Index
	LDA #$00
	STA !barrelSwapTimer
	LDA #$02
	STA !barrelSwapDirection
	RTL
	
Barrel1Hit:
	LDX !barrel1Index
	LDA #$04
	PHA
	BRA BarrelHit
	RTL
	
Barrel2Hit:
	LDX !barrel2Index
	LDA #$FC
	PHA
BarrelHit:
	LDA #$E0
	STA !barrelYSpeed
	PLA
	STA !barrelXSpeed
	LDA #$01
	STA !barrelFallThroughGround
	RTL
	
	
BarrelSineTable:
db $00, $01, $02, $02, $03, $04, $05, $05, $06, $07, $08, $08, $09, $0A, $0A, $0B 
db $0B, $0C, $0C, $0D, $0D, $0E, $0E, $0E, $0F, $0F, $0F, $10, $10, $10, $10, $10 
db $10, $10, $10, $10, $10, $10, $0F, $0F, $0F, $0E, $0E, $0E, $0D, $0D, $0C, $0C 
db $0B, $0B, $0A, $0A, $09, $08, $08, $07, $06, $05, $05, $04, $03, $02, $02, $01 
db $00, $FF, $FE, $FE, $FD, $FC, $FB, $FB, $FA, $F9, $F8, $F8, $F7, $F6, $F6, $F5 
db $F5, $F4, $F4, $F3, $F3, $F2, $F2, $F2, $F1, $F1, $F1, $F0, $F0, $F0, $F0, $F0 
db $F0, $F0, $F0, $F0, $F0, $F0, $F1, $F1, $F1, $F2, $F2, $F2, $F3, $F3, $F4, $F4 
db $F5, $F5, $F6, $F6, $F7, $F8, $F8, $F9, $FA, $FB, $FB, $FC, $FD, $FE, $FE, $FF 	
	
	
BarrelMAIN:
	LDA !barrelSwapDirection
	BEQ .notSwapping

	
	LDA !barrelSwapDirection
	DEC
	ASL #6
	CLC
	ADC !barrelSwapTimer
	AND #$7F
	TAY
	LDA BarrelSineTable,y
	CLC
	ADC #$40
	STA !barrelYPos
	
	LDA !barrelSwapDirection
	DEC
	ASL #6
	CLC
	ADC !barrelSwapTimer
	AND #$7F
	LSR
	TAY
	LDA BarrelSineTable,y
	ASL
	CLC
	ADC #$60
	STA !barrelXPos	
	
	LDA !barrelSwapTimer
	INC
	STA !barrelSwapTimer
	CMP #$40
	BNE .dontStopSwapping
	
	LDA #$00
	STA !barrelSwapTimer
	STA !barrelSwapDirection
	LDA #$40
	STA !barrelYPos
	


.dontStopSwapping
.notSwapping
	LDA !barrelDontMove
	BNE .dontFall
	LDA !barrelFallThroughGround
	BNE .skipGroundCheck
	LDA $14D4,x
	BMI .skipGroundCheck
	LDA !barrelYPos
	CMP #$40
	BCS .dontFall
.skipGroundCheck
	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position if it's falling.
.dontFall

	LDA !barrelFallThroughGround
	BEQ +
	LDA !barrelYPos
	BPL +
	JSL DeleteActor
	RTL
	
	+


	LDA !barrelXPos
	STA $00
	LDA !barrelYPos
	STA $01
	LDA #$2D
	STA $02
	LDA #$34
	STA $03
	LDA #$02
	JSL DrawTile
	
	LDA $00
	CLC : ADC #$08
	STA $00
	LDA #$2E
	STA $02
	LDA #$02
	JSL DrawTile
	
	LDA !barrelXPos
	STA $00
	LDA $01
	CLC : ADC #$10
	STA $01
	LDA #$4D
	STA $02
	LDA #$02
	JSL DrawTile
	
	LDA $00
	CLC : ADC #$08
	STA $00
	LDA #$4E
	STA $02
	LDA #$02
	JSL DrawTile
	
	RTL
	
BarrelNMI:
	RTL
	
	
	
	
	
	
	
	
	