MainOWInit:
	RTS
YoshislandInit:
	RTS
VanillaDomeInit:
	RTS
ForestIllusionInit:
	REP #$20
	LDA #$0000
	STA $7FB5FE
	SEP #$20

	LDA #$01
	STA $3E

	LDX #$12
.Lay3Loop
	LDA .Layer3HDMA,x
	STA $7FB600,x
	DEX
	BPL .Lay3Loop

	REP #$10
	LDX.w #BowserofValleyInit-.OWClouds-1
.STIMUpload
	LDA .OWClouds,x
	STA $7F837D,x
	DEX
	BPL .STIMUpload
	SEP #$10

	LDA #$00
	STA $4330
	LDA #$02
	STA $4340

	LDA #$32
	STA $4331
	STA $4341

	REP #$20
	LDA.w #.Table2
	STA $4332
	LDA.w #.Table1
	STA $4342

	SEP #$20

	LDA.b #.Table2>>16
	STA $4334
	LDA.b #.Table1>>16
	STA $4344

	LDA #$01
	STA $4350

	LDA #$2C
	STA $4351

	REP #$20
	LDA.w #.SubMainHDMA
	STA $4352
	SEP #$20

	LDA.b #.SubMainHDMA>>16
	STA $4354

	LDA #$02
	STA $4360

	LDA #$11
	STA $4361

	REP #$20
	LDA.w #$B600
	STA $4362
	SEP #$20

	LDA.b #$7F
	STA $4364

	LDA #$78
	TSB $0D9F
	
; medicode
	
	REP #$20
	LDA #$0060
	STA !medic_XPos
	LDA #$0180
	STA !medic_YPos
	SEP #$20
	
	RTS

.Table1
	db $24,$25,$89
	db $08,$26,$89
	db $04,$26,$8A
	db $05,$27,$8A
	db $07,$27,$8B
	db $01,$28,$8B
	db $06,$28,$8C
	db $06,$28,$8D
	db $01,$29,$8D
	db $06,$29,$8E
	db $04,$29,$8F
	db $03,$2A,$8F
	db $05,$2A,$90
	db $03,$2B,$91
	db $01,$2B,$92
	db $02,$2C,$92
	db $01,$2C,$93
	db $01,$2D,$93
	db $03,$2D,$94
	db $03,$2E,$95
	db $03,$2F,$96
	db $01,$2F,$97
	db $02,$30,$97
	db $02,$30,$98
	db $01,$31,$98
	db $02,$31,$99
	db $01,$32,$99
	db $06,$32,$9A
	db $03,$32,$9B
	db $03,$33,$9B
	db $06,$33,$9C
	db $06,$34,$9D
	db $04,$34,$9E
	db $02,$35,$9E
	db $06,$35,$9F
	db $08,$36,$9F
	db $08,$37,$9F
	db $08,$38,$9F
	db $07,$39,$9F
	db $07,$3A,$9F
	db $07,$3B,$9F
	db $07,$3C,$9F
	db $07,$3D,$9F
	db $06,$3E,$9F
	db $1D,$3F,$9F
	db $00

.Table2
	db $29,$48
	db $0C,$49
	db $0C,$4A
	db $0C,$4B
	db $0C,$4C
	db $05,$4D
	db $03,$4E
	db $04,$4F
	db $03,$50
	db $04,$51
	db $04,$52
	db $03,$53
	db $06,$54
	db $0A,$55
	db $08,$56
	db $0A,$57
	db $09,$58
	db $08,$59
	db $07,$5A
	db $08,$5B
	db $07,$5C
	db $08,$5D
	db $06,$5E
	db $2E,$5F
	db $00

.Layer3HDMA
	db $58 : dw $0000
	db $10 : dw $0000
	db $20 : dw $0000
	db $20 : dw $0000
	db $1C : dw $0000
	db $14 : dw $0000
	db $00

.SubMainHDMA
	db $58,$14,$03
	db $6F,$10,$07
	db $14,$04,$13
	db $00

.OWClouds
	incbin OWClouds.stim

BowserofValleyInit:
	RTS
SpecialWorldInit:
	RTS
StarworldInit:
	RTS
