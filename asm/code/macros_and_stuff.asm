	!RAM_SpriteTilesReserved	= $18CA

macro hdma_vscroll_init(rom_table, ram_table, modded_entry_addr, data_difference)
	LDA.w #(<ram_table>)
	STA <modded_entry_addr>
	CLC : ADC.w #<data_difference>
	STA <modded_entry_addr>+<data_difference>
endmacro

macro hdma_vscroll_restore_last(ram_table, rom_table, modded_entry_addr, data_alternator, second_table)
	LDA <modded_entry_addr>,x
	STA $00
	CLC : ADC.w #((<rom_table>)-(<ram_table>))
	SEC : SBC <data_alternator>
	STA $03
	LDA [$03]
	STA [$00]
 if <second_table>
	LDA [$03],y
	STA [$00],y
 endif
endmacro

macro hdma_vscroll_main(entry_size, second_table)
?Loop:	LDA [$00]				; Get number of scanlines in this entry (+ next byte, which we don't need)
	AND #$00FF				; Wipe high byte, so we've only got the number of scanlines
	DEC					; Subtract 1.
	SEC					; \ Subtract our
	SBC $03					; / "remaining scanlines" value
	BCS ?Use				; if we don't still have more to go in the table, break out of loop
	EOR #$FFFF				; \ Basically, the effective result of this is to set the number
	STA $03					; / of scanlines left to the previous value minus the number in this entry
	%incdec16dp($00, (<entry_size>)+1)	; Offset our indirect pointer at A to point to the next entry
	BRA ?Loop				; loop
?Use:	SEP #%00100000				; 8-bit A
	INC					; + 1
	STA [$00]				; Temporary v-scroll scanline count value for this entry.
 if <second_table>
	STA [$00],y
 endif
	REP #%00100000				; Back to 16-bit A
endmacro

macro incdec16dp(dp, n)
 if (<n>) < -2
	LDA <dp>
	SEC
	SBC.w #-(<n>)
	STA <dp>
 elseif (<n>) == -2
	DEC <dp>
	DEC <dp>
 elseif (<n>) == -1
	DEC <dp>
 elseif (<n>) > 2
	LDA <dp>
	CLC
	ADC.w #(<n>)
	STA <dp>
 elseif (<n>) == 2
	INC <dp>
	INC <dp>
 elseif (<n>) == 1
	INC <dp>
 endif
endmacro