MainOWcode:
	RTS
Yoshislandcode:
	RTS
VanillaDomecode:
	RTS
	
!medic_Apex = $1500
!medic_Timer = $1501
!medic_YPos = $1504
!medic_XPos = $1502
!medic_Direction = $1506
	
ForestIllusioncode:
	REP #$20
	LDA $7FB5FE
	DEC A
	STA $7FB5FE
	;LSR A
	STA $7FB60D
	LSR A
	STA $7FB60A
	LSR A
	STA $7FB607
	LSR A
	STA $7FB604
	SEP #$20
	
; medicode	

	LDA $1F19
	CMP #$B8
	BCS +
	LDA $1F17
	CMP #$40
	BCC +
	CMP #$B0
	BCS +
	LDA !medic_YPos
	CMP #$80
	BEQ ++
+
INC !medic_Timer
LDA !medic_Timer
CMP #$08 ;                     <<<< it here
BNE ++
LDA !medic_Apex
BEQ +
DEC !medic_Apex
LDA #$07
STA !medic_Timer
BRA ++
+
STZ !medic_Timer
LDA !medic_Direction
INC
AND #$01
TAY
LDA !medic_YPos
SEC : SBC Acceleration,y
STA !medic_YPos
CMP Limit2,y
BNE ++
INC !medic_Direction
LDA #$02
STA !medic_Apex
++
	JSR GetScreenPos
	JSR SetOAMIndex
	LDX #$0B
	Loop:
	LDA BlimpTiles,x
	STA $0302,y
	LDA #$36
	STA $0303,y
	LDA $00
	CLC : ADC XOffset,x
	STA $0300,y
	LDA $02
	CLC : ADC YOffset,x
	STA $0301,y
	PHY
	TYA
	LSR A
	LSR A
	TAY
	LDA TileSize,x
	STA $0460,y
	PLY
	INY #4
	DEX
	BPL Loop
	RTS

Acceleration:
	db $01,$FF
;Limit:
;	db $02,$FE
Limit2:
	db $7D,$81 ;              <<<< it also here

BlimpTiles:
	db $A0,$A2,$A4,$A6
	db $C0,$C2,$C4,$C6
	db $A8,$B8,$C8,$D8

XOffset:
	db $00,$10,$20,$30
	db $00,$10,$20,$30
	db $10,$18,$20,$28	

YOffset:
	db $00,$00,$00,$00
	db $10,$10,$10,$10
	db $20,$20,$20,$20

TileSize:
	db $02,$02,$02,$02
	db $02,$02,$02,$02
	db $00,$00,$00,$00
	
GetScreenPos:
	LDY #$02                
	JSR +
	LDY #$00                
+           
	REP #$20
	LDA !medic_XPos,y                  ; Accum (16 bit) 
	SEC                       
	SBC $001A,y 
	STA $0000,y             
	SEP #$20                  ; Accum (8 bit) 
	RTS                       ; Return 

SetOAMIndex:
	LDY #$DC
.OAMLoop:
	LDA $02FD,y
	CMP #$F0
	BNE .GetOAM
	CPY #$40
	BEQ .GetOAM
	DEY
	DEY
	DEY
	DEY
	BRA .OAMLoop
.GetOAM:
	RTS
			



BowserofValleycode:
	RTS
SpecialWorldcode:
	RTS
Starworldcode:
	RTS
	
