header
lorom

;!Freespace = $2CC600 ;(was 37ff00)

org $01F71D
		autoclean JML RunAway
		NOP

org $02A481
		autoclean JML RunAwayExt
		NOP

freecode

Start:
RunAway:
	LDA $7C
	BEQ +
	JSL $80F5B7
	JML $81F74B
+		 
	LDA #$13
	STA $1DFC
	JML $81F722

RunAwayExt:
	LDA $7C
	BEQ +
	JSL $80F5B7
	JML $82A4AC
+	
	LDA #$13
	STA $1DFC
	JML $82A486

End: