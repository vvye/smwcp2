;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; JSL.ips
; 頻繁に使用するルーチンを.ips化して
; スプライト利用者の負担を軽減しようとしてみるテスト
; この領域使ってるツール類とぶつかったら元も子もないね！
;
; 各ルーチンの仕様はJSL.asmと同様
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

header
lorom

		!JSL_i =	$2CC400
		!SubOffScreen =	!JSL_i
		!GetDrawInfo =	!JSL_i+$04
		!SubHorzPos =	!JSL_i+$08
		!SubVertPos =	!JSL_i+$0C

		org !JSL_i
		JML START_SUB
		JML GetDrawInfo
		JML SubHorzPos

SubVertPos:	LDY #$00
		LDA $96
		SEC
		SBC $D8,x
		STA $0E
		LDA $97
		SBC $14D4,x
		BPL .IfPlus
		INY
.IfPlus		RTL

START_SUB:	STA $00
		LDA $15A0,x
		ORA $186C,x
		BEQ .Return2
		LDA $167A,x
		AND #$04
		BNE .Return2
		LDA $5B
		LSR A
		BCS .VerticalLevel
		LDA $D8,x
		ADC #$50
		LDA $14D4,x
		ADC #$00
		CMP #$02
		BCS .EraseSprite
		LDY $00
		LDA $14E0,x
		XBA
		LDA $E4,x
		REP #$21
		PHB
		PHK
		PLB
		ADC .AddTable,y
		SEC
		SBC $1A
		CMP .CmpTable,y
		PLB
.Common		SEP #$20
		BCC .Return2
.EraseSprite	LDA $14C8,x
		CMP #$08
		BCC .KillSprite
		LDY $161A,x
		CPY #$FF
		BEQ .KillSprite
		LDA $1938,y
		AND #$FE
		STA $1938,y
.KillSprite	STZ $14C8,x
.Return2	RTL

.VerticalLevel	LDA $13
		LSR A
		BCS .CheckY
		LDA $14E0,x
		XBA
		LDA $E4,x
		REP #$21
		ADC.w #$0040
		CMP.w #$0280
		BRA .Common
.CheckY		LDA $14D4,x
		XBA
		LDA $D8,x
		REP #$20
		SBC $1C
		CLC
		ADC.w #$0050
		CMP.w #$01B0
		BRA .Common

.AddTable	dw $0040,$0040,$0010,$0070
		dw $0090,$0050,$0080,$FFC0
.CmpTable	dw $0170,$01E0,$01B0,$01D0
		dw $0230,$01B0,$0220,$0160

GetDrawInfo:	STZ $15A0,x
		LDA $E4,x
		CMP $1A
		LDA $14E0,x
		SBC $1B
		BEQ .NoScreenX
		INC $15A0,x
.NoScreenX	STZ $186C,x
		TXY
		LDX #$00
		LDA $1662,y
		AND #$20
		BEQ .OnScreenLoop
		LDX #$02
.OnScreenLoop	LDA $14D4,y
		XBA
		LDA $00D8,y
		REP #$21
		ADC.l .Table1,x
		SEC
		SBC $1C
		SEP #$21
		XBA
		BEQ .OnScreenY
		LDA $186C,y
		ORA.l .Table2,x
		STA $186C,y
.OnScreenY	DEX #2
		BPL .OnScreenLoop
		TYX
		LDA $14E0,x
		XBA
		LDA $E4,x
		REP #$21
		ADC.w #$0040
		SEC
		SBC $1A
		CMP.w #$0180
		SEP #$20
		LDA #$80
		ROL A
		STA $15C4,x
		BNE .Invalid
		LDY $15EA,x
		LDA $E4,x
		SBC $1A
		STA $00
		LDA $D8,x
		SEC
		SBC $1C
		STA $01
		RTL

.Invalid	REP #$20
		PLA
		PLY
		PLA
		PHY
		PHA
		SEP #$20
		RTL

.Table1		dw $000C,$001C
.Table2		db $01,$00,$02,$00

SubHorzPos:	LDY #$00
		LDA $94
		SEC
		SBC $E4,x
		STA $0F
		LDA $95
		SBC $14E0,x
		BPL .IfPlus
		INY
.IfPlus		RTL