header
lorom

!Freespace = $268100
!ramaddress = $7FABAA
!ramaddress2 = $0DD9
!ramaddress3 = $0DDB

org $019140
autoclean JML Main
NOP #2

org $019457
autoclean JSL YOffset

org $01946E
autoclean JSL XOffset

org $0194C1
autoclean JSL YOffset

org $0194DF
autoclean JSL XOffset

org $01936A
autoclean JSL Slope
NOP

org $01940A
autoclean JSL Customfix


freecode

YOffset:
PHB
PHK
PLB		;use current bank

PHX
PHA		;push A (sprite position) and X (sprite index)
LDA $1656,x
AND #$0F
CMP #$0F	;$0F = custom object clipping
BEQ CustomY
PLA
TYX		;pull A and use original index to object clipping table...
CLC
ADC $0190F7,x	;...to load original values
PLX
PLB		;back to previous bank
RTL

XOffset:
PHB
PHK
PLB		;use current bank

PHX
PHA		;push A (sprite position) and X (sprite index)
LDA $1656,x
AND #$0F
CMP #$0F	;$0F = custom object clipping
BEQ CustomX
PLA
TYX		;pull A and use original index to object clipping table (Y)...
CLC
ADC $0190BA,x	;...to load original values
PLX
PLB		;back to previous bank
RTL

CustomY:
LDA !ramaddress,x
ASL
CLC
ADC !ramaddress,x
ASL			; *6
TAY		;use custom index
LDA $B6,x
BPL sprtgoesrightthere
INY
INY
INY
sprtgoesrightthere:
LDA $0F
CMP #$02
BCC horzsoreturn
INY
LDA $AA,x
BPL sprtgoesdownthere
INY
sprtgoesdownthere:
horzsoreturn:

LDA #$00
LDX !ramaddress2	;save width value
PHY
LDY $0F
CPY #$02
BCS nothorz	;if it is horinzontal clipping, add width
TXA
nothorz:
PLY
CLC
ADC YTable,y	;add custom Y offset
BMI substractY
STA !ramaddress2	;use width as a freeram for addition
PLA			;pull sprite position
CLC
ADC !ramaddress2	;add offsets

STX !ramaddress2	;restore width value
PLX			;restore sprite index

PLB			;back to previous bank
RTL

substractY:
EOR #$FF
INC			;two's complement
STA !ramaddress2	;use width as a freeram for addition
LDA $04,s
CLC
ADC #$0B
STA $04,s		;modify return address

PLA
SEC
SBC !ramaddress2

STX !ramaddress2	;restore height value
PLX			;restore sprite index

STA $0C
AND.b #$F0
STA $00
LDA $14D4,x
SBC #$00		;hijacked restore/modify

PLB
RTL



CustomX:
LDA #$00		;see above for this part
LDX !ramaddress3
PHY
LDY $0F
CPY #$02
BCC notvert
TXA
notvert:
PLY
CLC
ADC XTable,y
BMI substractX
STA !ramaddress3
PLA			;pull sprite position
CLC
ADC !ramaddress3	;add offsets

STX !ramaddress3	;restore height value
PLX			;restore sprite index

PLB
RTL

substractX:
EOR #$FF
INC
STA !ramaddress3
LDA $04,s
CLC
ADC #$09
STA $04,s

PLA
SEC
SBC !ramaddress3

STX !ramaddress3	;restore height value
PLX			;restore sprite index

STA $0A
STA $01
LDA $14E0,x
SBC #$00

PLB
RTL

Slope:
PHA
LDA $15B8,x
BNE donotwriteslope	; if sprite was already marked as "on slope", don't overwrite it
PLA
STA $15B8,x
CMP #$04
RTL
donotwriteslope:
PLA
CMP #$04
RTL

Customfix:
PHY
PHX
LDA $1656,x
AND #$0F
CMP #$0F	;$0F = custom object clipping
BEQ Offsetfix
PLX
PLY
LDA $D8,x
AND.b #$F0
RTL

Offsetfix:

LDA !ramaddress,x
ASL
CLC
ADC !ramaddress,x
ASL
TAY
LDA $B6,x
BPL sprtgoesrightherehere
INY
INY
INY
sprtgoesrightherehere:
INY

LDA $D8,x
PHA
LDA $04,s
CLC
ADC #$04
STA $04,s		; modify return address
LDX !ramaddress2	; use freeram

PHB
PHK
PLB		;use current bank
LDA YTable,y
AND #$0F
STA !ramaddress2
BEQ dontmodify
LDA #$10
SEC
SBC !ramaddress2
STA !ramaddress2
dontmodify:
PLB

PLA
SEC
SBC !ramaddress2
AND #$F0
CLC
ADC !ramaddress2
CLC
ADC $1694

STX !ramaddress2
PLX

CMP #$D0
BCC dontclearhigh
STZ $14D4,x
dontclearhigh:

PLY
RTL



Main:
LDA $1656,x
AND #$0F
CMP #$0F
BEQ Customobjectclip	; 0F = custom object clipping
STZ $1694
STZ $1588,x
JML $819146	; restore and return


Customobjectclip:

PHY
STZ $15B8,x
STZ $1588,x

PHB
PHK
PLB
LDA !ramaddress,x
ASL
CLC
ADC !ramaddress,x
ASL
TAY
LDA $B6,x
BPL sprtgoesright
INY
INY
INY
sprtgoesright:
LDA widthheight,y
STA !ramaddress2
INY
LDA $AA,x
BPL sprtgoesdown
INY
sprtgoesdown:
LDA widthheight,y
STA !ramaddress3
PLB

loop:

STZ $1694
STZ $185E
LDA $164A,x
STA $1695
STZ $164A,x

PHK			;push return bank
PER ReturnAddress-1	;push return address minus 1
PHB			;push data bank
LDA #$01		;bank 01
PHA		        ;push A on the stack
PLB			;new data bank (bank 01)
PEA $913D		;push return address minus 1, containing PLB + RTL
JML $819155		;long jump to local routine ending with RTS
ReturnAddress: 		;Return here



LDA !ramaddress2
ORA !ramaddress3
BEQ exitloop
LDA !ramaddress2
BPL substractheight
CLC
ADC #$10
BCS Xoutbound
readytogoY:
STA !ramaddress2
BRA goY

substractheight:
SEC
SBC #$10
BCS readytogoY
Xoutbound:
STZ !ramaddress2
goY:

LDA !ramaddress3
BPL substractwidth
CLC
ADC #$10
BCS Youtbound
readytogoloop:
STA !ramaddress3
BRA goloop

substractwidth:
SEC
SBC #$10
BCS readytogoloop
Youtbound:
STZ !ramaddress3
goloop:

BRA loop

exitloop:
PLY
JML $81920B	;back


;Format: R,DR,UR,L,DL,UL

widthheight:
db $32,$10,$27,$32,$F0,$D9	;custom object clipping index (to store at !ramaddress): 0
db $00,$00,$00,$00,$00,$00	;1
db $00,$00,$00,$00,$00,$00	;2
db $2E,$10,$27,$2E,$F0,$D9	;...
db $32,$10,$27,$32,$F0,$D9

YTable:
db $00,$40,$00,$00,$40,$00	;0
db $00,$30,$00,$00,$30,$00	;1
db $00,$30,$00,$00,$30,$00	;...
db $FC,$38,$F8,$FC,$38,$F8
db $E8,$28,$E8,$E8,$28,$E8

XTable:
db $3E,$18,$18,$02,$28,$28	;0
db $00,$10,$00,$00,$10,$00	;1
db $00,$10,$00,$00,$10,$00	;...
db $2E,$08,$08,$F2,$18,$18
db $16,$00,$00,$EA,$10,$10