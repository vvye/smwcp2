header
lorom

org $20CA4F

GET_DRAW_INFO:		STZ $186C,x
			STZ $15A0,x
			LDA $E4,x
			CMP $1A
			LDA $14E0,x
			SBC $1B
			BEQ ON_SCREEN_X
			INC $15A0,x

ON_SCREEN_X:		LDA $14E0,x
			XBA
			LDA $E4,x
			REP #$20
			SEC
			SBC $1A
			CLC
			ADC #$0040
			CMP #$0180
			SEP #$20
			ROL A
			AND #$01
			STA $15C4,x
			BNE INVALID

			PHB
			PHK
			PLB
			LDY #$00
			LDA $1662,x
			AND #$20
			BEQ ON_SCREEN_LOOP
			INY
ON_SCREEN_LOOP:		LDA $D8,x
			CLC
			ADC SPR_T1,y
			PHP
 			CMP $1C
			ROL $00
			PLP
			LDA $14D4,x
			ADC #$00
			LSR $00
			SBC $1D
			BEQ ON_SCREEN_Y
			LDA $186C,x
			ORA SPR_T2,y
			STA $186C,x
ON_SCREEN_Y:		DEY
			BPL ON_SCREEN_LOOP

			LDY $15EA,x
			LDA $E4,x
			SEC
			SBC $1A
			STA $00
			LDA $D8,x
			SEC
			SBC $1C
			STA $01
			PLB
			RTL

INVALID:		REP #$20
			PLA
			PLY
			PLA
			PHY
			PHA
			SEP #$20
			RTL

SPR_T1:			db $0C,$1C
SPR_T2:			db $01,$02