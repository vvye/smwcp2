db $42

!Sound = $08		; Set to 00 for no sound effect.
!Port = $1DF9

!Item = $78		; Sprite #.
!ISCUSTOM = $00		; Set to 01 to generate custom sprite.
!INITSTAT = $01		; Set this to 09 ONLY if generating a shell/stationary object.

;Displacement and speed.
!XDISP = $0000
!YDISP = $FFD5
!XSPD = $00
!YSPD = $00

JMP Main : JMP Main : JMP Main : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

Main:
	LDA $187A
	BNE +
	JMP Return
+	LDA #$80
	STA $18
	BRA skip
Sprite:
LDA $0A ;\ 
AND #$F0 ;| Update the position
STA $9A ;| of the block
LDA $0B ;| so it doesn't shatter
STA $9B ;| where the players at
LDA $0C ;|
AND #$F0 ;|
STA $98 ;|
LDA $0D ;|
STA $99 ;/

skip:
	LDX #$0B
Loop:
	LDA $14C8,x
	BEQ Next
	LDA $9E,x
	CMP #$35
	BEQ Found
	CMP #$2D
	BEQ Found
Next:
	DEX
	BPL Loop
	RTL

Found:
	LDA #$04
	STA $14C8,x
	LDA #$1F
	STA $1540,x
	JSL $07FC3B
	LDA #!Sound
	STA !Port

	PHY		;preserve Y
	JSL $02A9DE	;grab sprite slot
	BMI Return	;return if none avaliable
	TYX		;into X

	LDA #!INITSTAT	;set to initial status
	STA $14C8,x	;status

	LDA #!ISCUSTOM	;if custom set different type table
	BNE Custom

	LDA #!Item	;sprite to generate
	STA $9E,x		;sprite type
	JSL $07F7D2		;clear
	BRA SkipCustom
  
Custom:
	LDA #!Item
	STA $7FAB9E,x	;custom sprite type table
	JSL $07F7D2	;clear
	JSL $0187A7	;load tables
        LDA #$88		;set as custom sprite
        STA $7FAB10,X
		
SkipCustom:

	REP #$20	;apply xdisp
	LDA $9A
	CLC
	ADC #!XDISP
	SEP #$20
	STA $E4,x
	XBA
	STA $14E0,x

	REP #$20	;apply ydisp
	LDA $98
	CLC
	ADC #!YDISP
	SEP #$20
	STA $D8,x
	XBA
	STA $14D4,x

	LDA #!XSPD	;store speeds
	STA $B6,x
	LDA #!YSPD
	STA $AA,x

	LDA #$02	;erase self
	STA $9C
	JSL $00BEB0	;generate blank block
	PLY
Return:
	RTL