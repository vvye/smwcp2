db $42
JMP Main : JMP Main : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

Main:
	JSR SetSpriteCoord1
	LDA #$74
	JSR SpawnSprite
	PHY
	LDY #$03
	LDA $7F                   
        ORA $81                   
        BNE NoGlitter
        LDY #$03
	
LoopStart:
        LDA $17C0,y
        BEQ Go
	DEY
	BPL LoopStart
	BRA NoGlitter
	
Go:
	LDA #$05
        STA $17C0,y
        LDA $9A
        AND #$F0
        STA $17C8,y
        LDA $98
        AND #$F0
        STA $17C4,y
        LDA $1933
        BEQ Layer1
        LDA $9A
        SEC
        SBC $26
        AND #$F0
        STA $17C8,y
        LDA $98
        SEC
        SBC $28
        AND #$F0
        STA $17C4,y
Layer1:
        LDA #$10
        STA $17CC,y
	
NoGlitter:
	LDA #$02
	STA $9C
	JSL $00BEB0
	
	REP #$20
	LDA $98
	SEC
	SBC #$10
	STA $98
	SEP #$20
	
	LDA #$02
	STA $9C
	JSL $00BEB0
	PLY
	
Return:
	RTL
	
	SpawnSprite: 
	PHX 
	PHP 
	SEP #$30 
	PHA 
	LDX #$0B 
SpwnSprtTryAgain: 
	LDA $14C8,x ;Sprite Status Table 
	BEQ SpwnSprtSet 
	DEX 
	CPX #$FF 
	BEQ SpwnSprtClear 
	BRA SpwnSprtTryAgain 
SpwnSprtClear: 
	DEC $1861 
	BPL SpwnSprtExist 
	LDA #$01 
	STA $1861 
SpwnSprtExist: 
	LDA $1861 
	CLC 
	ADC #$0A 
	TAX 
SpwnSprtSet: 
	STX $185E 
	LDA #$08 
	STA $14C8,x ;Sprite Status Table 
	PLA 
        STA $9E,x ;Sprite Type   	    
        JSL $07F7D2             ; reset sprite properties
	INC $15A0,x ;"Sprite off screen" flag, horizontal 
	LDA $9A ;Sprite creation: X position (low) 
	STA $E4,x ;Sprite Xpos Low Byte 
	LDA $9B 
	STA $14E0,x ;Sprite Xpos High Byte 
	LDA $98 ;Sprite creation: Y position (low) 
	STA $D8,x ;Sprite Ypos Low Byte 
	LDA $99 ;Sprite creation: Y position (high) 
	STA $14D4,x ;Sprite Ypos High Byte 
	LDA #$2C 
	STA $144C,x 
	LDA $190F,x ;Sprite Properties 
	BPL SpwnSprtProp 
	LDA #$10 
	STA $15AC,x 
SpwnSprtProp: 
	PLP 
	PLX 
	RTS 

SetSpriteCoord1: 
	PHP 
	REP #$20 
	LDA $98 
	AND #$FFF0 
	STA $98 
	LDA $9A 
	AND #$FFF0 
	STA $9A 
	PLP 
	RTS 