db $42
JMP MAIN : JMP MAIN : JMP MAIN : JMP RETURN : JMP RETURN : JMP RETURN : JMP RETURN : JMP RETURN : JMP RETURN : JMP RETURN

!EVENTACTIVE = $80
!FREESPACE = $7F9A84

MAIN:	LDA $1F0E		; Get level clearing flags EVENTS (60 - 67)

	AND #!EVENTACTIVE	; Check for Event 96 *May Need To change depending on Event Number of 131*

	BNE RETURN		; If you've finished the level, don't clear the flags
	LDA $13CE		; Get midway bar hit flag
	BNE RETURN		; If you've hit the midpoint, don't clear the flags
	LDA $141A		; Get number of times Level 131 was entered
	BNE RETURN		; If this isn't the first time, don't do it.
	LDA #$00
	STA !FREESPACE		; Clear Base Flags
RETURN:	RTL
