db $42

JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball : JMP TopCorner : JMP HeadInside : JMP BodyInside

MarioAbove:
TopCorner:
	LDA #$B0
	STA $7D
	RTL
MarioBelow:
	LDA #$50
	STA $7D
	RTL
MarioSide:
	LDA #$E6
	STA $7D

	LDA $7B
	EOR #$FF
	INC A
	STA $7B
	LDA $7B
	BPL GoRight

	REP #$20
	LDA $94
	SEC
	SBC #$0008
	STA $94
	SEP #$20
	RTL

GoRight:
	REP #$20
	LDA $94
	CLC
	ADC #$0008
	STA $94
	SEP #$20
	RTL
SpriteV:
	LDA $14C8,x
	CMP #$08
	BCC Return

	LDA $AA,x
	CMP #$80
	BCS BounceDown
	LDA #$B0
	STA $AA,x
	RTL
BounceDown:
	LDA #$40
	STA $AA,x
Return:
	RTL

SpriteH:
	REP #$20
	LDA $00
	PHA
	SEP #$20

	LDA $E4,x
	STA $00
	LDA $14E0,x
	STA $01

	LDA #$F0
	STA $AA,x

	LDA $B6,x
	EOR #$FF
	INC A
	STA $B6,x
	LDA $B6,x
	BPL SGoRight

	REP #$20
	LDA $00
	SEC
	SBC #$0008
	STA $00
	SEP #$20

	LDA $00
	STA $E4,x
	LDA $01
	STA $14E0,x

	REP #$20
	PLA
	STA $00
	SEP #$20
	RTL

SGoRight:
	REP #$20
	LDA $00
	CLC
	ADC #$0008
	STA $00
	SEP #$20

	LDA $00
	STA $E4,x
	LDA $01
	STA $14E0,x

	REP #$20
	PLA
	STA $00
	SEP #$20
	RTL
HeadInside:
BodyInside:
MarioCape:
MarioFireball:
RTL