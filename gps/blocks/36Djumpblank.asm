;Act like a blank tile if Jumped block. If Mario jumps again, it will act like the muncher block.
;Use it with levelASM
;Acts like = 25

db $42
JMP Start : JMP Start : JMP Side : JMP Start : JMP Start : JMP Return : JMP Start : JMP Start : JMP Start : JMP Start

Start:
LDA $14AF
sta $00
ldy $7C
beq +
	eor #$01
	sta $00
+
lda $00
bne act_like_spike
bra act_like_block

act_like_spike:
LDY #$01
LDA #$2F
STA $1693
rtl

Side:
act_like_block:
LDY #$01
LDA #$30
STA $1693
Return:
rtl

