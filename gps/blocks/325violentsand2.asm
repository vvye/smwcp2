db $42
JMP Main : JMP Main : JMP Main : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

!Power = $00
!Sink = $09

Main:
	STZ $15		;Stop controller movements.

	LDA #$02	;
	STA $1471	;What type of platform Mario is on.

	LDA $87		;
	AND #$01	; 
	BNE NoSound	;If sound already played, branch.

	LDA #$25	;Play sinking sound.
	STA $1DF9	;Store to bank.
NoSound:
	LDA $87		;
	ORA #$01	;
	STA $87		;Set bit so sound doesn't replay.

	LDA #$09	;Load horizontal speed.
	STA $7B		;store.
	RTL
Sprite:
	LDA $7FAB9E,x		
	CMP #$61		;\ If the sprite is the right one...
	BEQ Solid		;/ branch
Sink:
	LDA #!Sink
	STA $AA,x
	STA $BB,x
	RTL
Solid:
	LDY #$01                ;|Load "Tilemap Page": 0x1
	LDA #$30                ;\And Load Tile: 0x30 (Cement Block)
	STA $1693               ;|And store to Block Status Map 
Right:
	RTL
Return:
	RTL		;Return