;Shatter if a sprite lands on the block and shakes layer 1.
;Acts like = 130
;Block # = 36B
db $42
JMP Return : JMP Return : JMP Return : JMP Start : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Start:
LDA $1887
BEQ Return
LDA $0A
AND #$F0
STA $9A
LDA $0B
STA $9B
LDA $0C
AND #$F0
STA $98
LDA $0D
STA $99
LDA #$02
STA $9C
JSL $00BEB0
PHB
LDA #$02
PHA
PLB
LDA #$00
JSL $028663
PLB
Return:
RTL