Globals:
01  Miss9.txt
02  Game Over.txt
03  Boss Clear.txt
04  Stage Clear.txt
05  Starman.txt
06  P-switch.txt
07  Keyhole.txt
08  Iris Out.txt
09  muted.txt
Locals:
0A  Starman.txt
12  Intro Screen.txt
18  Title Screen.txt
1F  Desert Level 3.txt
21  Final Boss.txt
27  Forest Ghost House.txt
28  Final Castle.txt
2B  Grass Map.txt
31  Cave Level 1.txt
32  Dream Level 1.txt
33  Ice Level 3.txt
34  Dream Level 5.txt
35  Fire-Ice Map.txt
36  Sky Map.txt
37  Forest Level 5.txt
38  Reef Level 3.txt
39  Switch Palace.txt
3A  Oriental Level 1.txt
3C  Desert Map.txt
3D  Fire Level 1.txt
3E  Sky Ghost House.txt
3F  Oriental Map.txt
40  Forest Map.txt
41  Carnival Map.txt
42  Dream Map.txt
43  Reef Level 2.txt
44  Factory Level 1.txt
45  Sky Level 1.txt
46  Grass Level 3.txt
47  Sky Level 3.txt
48  Forest Level 4.txt
49  Grass Level 1.txt
4B  Grass Castle.txt
4C  Factory Castle.txt
4D  Ice Level 2.txt
4E  Fire Level 2.txt
4F  Sky Level 2.txt
51  Fire-Ice Castle.txt
52  Cave Level 2.txt
53  Forest Level 2.txt
54  Desert Castle.txt
55  Bonus Room.txt
56  Boss Battle.txt
57  Carnival Level 1.txt
58  Dream Level 2.txt
59  Desert Level 1.txt
5A  Cave Level 3.txt
5B  Forest Castle.txt
5C  Carnival Level 2.txt
5D  Factory Level 2.txt
5E  Desert Level 2.txt
5F  Reef Castle.txt
60  Carnival Castle.txt
61  Fire-Ice Level 1.txt
62  Oriental Level 3.txt
63  Forest Level 1.txt
64  Dream Level 3.txt
65  Fire-Ice Level 2.txt
66  Reef Level 1.txt
67  Ice Level 1.txt
68  Oriental Level 2.txt
69  Sky Castle.txt
6A  Grass Level 2.txt
6B  Dream Level 4.txt
6C  Oriental Level 4.txt
6D  Factory Map.txt
6E  Oriental Castle.txt
6F  Forest Level 3.txt
70  Reef Map.txt
71  Rush Time.txt
72  Carnival Level 3.txt
73  muted.txt
74  here we go.txt
75  Credits.txt
76  Cutscene_intro.txt
