import shutil
import subprocess

ROM = "SMWCP2.smc"

print("Dumping binary files in \"death/\" subdirectory...")

for x in xrange(1, 10):
    with open("Addmusic_list.txt", 'r') as list:
        list_lines = list.readlines()
        list_lines[1] = "01  Miss%d.txt\n" % x
    with open("Addmusic_list.txt", 'w') as list:
        for line in list_lines:
            list.write(line)
    subprocess.call(["AddmusicK.exe", ROM])
    shutil.copy2("asm/SNES/bin/music01.bin", "death/death%d.bin" % x)

subprocess.call(["asar.exe", "deathfix.asm", ROM])
raw_input("Done.")
