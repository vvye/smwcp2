#amk 2

#SPC
{
	#author "Jimmy52905"
	#comment "Forest/Swamp castle theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
     "@8@14.brr"
	"CTrumpet.brr"
}

#instruments {
     @9         $AF $B0 $00 $02 $A6 ;@30
     @29        $8F $E0 $00 $03 $00 ;@31
"@8@14.brr"     $AF $AD $00 $03 $00 ;@32
     @1         $8B $E0 $00 $03 $00 ;@33
"CPanFlute.brr" $8D $C0 $00 $04 $00 ;@34
     @6         $8F $F7 $00 $03 $00 ;@35
     @0         $BF $71 $00 $06 $00 ;@36
"CTrumpet.brr"  $8D $C0 $00 $06 $00 ;@37
     @10        $8F $E0 $00 $02 $D0 ;@38
}

#0 w200 t56
$EF $FD $22 $22 
$F1 $07 $52 $00
$F4 $02
y9v110@34$DE $0C $0C $20
o4
[r2]15
g+8e16r16g+8b16r16
/
o5
(10)[c+=120<g+8>c+8e8
     g+=28{f+32}g+=28{f+32}g+=32a8g+16r16e8g+16r16
     f+2a8g+16r16e8f+16r16
     g+=28{f+32}g+=28{f+32}g+=32a8g+16r16g+8e16r16
     d+4.e16d+16c+4.<g+8
     e8g+16r16>c+8e16r16g+=28{f+32}g+=28{f+32}g+=32
     g+=28{f+32}g+=28{d+32}e=32g+=28{g+32}a=28{d+32}e=32
     g+4.a16g+16f+2]1

y10v230@36
o3 l16
[c+<g+>ec+ef+g+eg+b>c+<g+>c+<b>d+c+
 ed+c+<g+>c+<baf+g+b>c+<baf+g+e
 <ab>c+<b>d+<a>d+c+ef+g+eg+f+ab
 >c+<baeg+f+ec+d+<b>ec+d+<bab]2

y9v110@34$DE $0C $0C $20
o5
(10)1
(11)[c+4d+4e4f+8.^24
     {f+32}g+8f+16r16f+16r16e16d+16c+4e4]1
<b4>d+=120f+8
<b4>d+=120r8
(11)1
f+4d+=120c+16d+16
q7db4q7ff+=120r8

y11v170@37$DE $10 $08 $28
(12)[<a4b4>c+4e4
     f+8e16r16e8c+16r16]1 <a4>c+4
<b4f+=120a8
b8r8>d+=120r8
(12)1 <a8b16r16>c+8e8
g+4e=120c+8
e4c+=120
y9v110@34$DE $0C $0C $20
o4b16r16

#1
y10v235@32
o4
(20)[c+8c+16c+16> c+8<c+8c+8c+8>c+8<c+8
     c+8c+16c+16>c+8<c+8c+8c+8>c+8<g+8
     <a8a16a16>a8e8<a8a8>a8e8
     <a8a16a16>a8e8<a4a4]2

/
(20)6
(22)[>c+8c+16c+16>c+8<c+8c+8c+8>c+8<c+8
     c+8c+16c+16>c+8<c+8c+8c+8>c+8<g+8
     <b8b16b16>b8f+8<b8b8>b8f+8
     <b8b16b16>b8f+8<b4b4]2
(23)[a8a16a16>a8e8<a8a8>a8e8
     <a8a16a16>a8e8<a4a4]1
(24)[b8b16b16>b8f+8]1 <b8b8>b8f+8
(24)1 <b4b4
(23)1
(25)[>c+8c+16c+16>c+8<c+8]1 c+8c+8>c+8<c+8
(25)1 c+4c+4

#2
y10v155@30
o5
(30)[c+4c+8.c+16{c+8c+8c+8}c+4
     c+4{c+8c+8c+8c+8c+8c+8c+8c+8c+8}
     <a4a8.a16{a8a8a8}a4
     a4{a8a8a8a4a4b4}]2
/
y9(30)6
(31)[>c+4c+8.c+16{c+8c+8c+8}c+4
     c+4{c+8c+8c+8c+8c+8c+8c+8c+8c+8}
     <b4b8.b16{b8b8b8}b4
     b4{b8b8b8b4b4b4}]2
(32)[a4a8.a16{a8a8a8}a4
     a4{a8a8a8a8a8a8a8a8a8}]1
b4b8.b16{b8b8b8}b4
b4{b8b8b8b4b4b4}
(32)1
>c+4c+8.c+16{c+8c+8c+8}c+4
c+4{c+8c+8c+8c+4c+4c+4}

#3 
y11v155@30
o4
(40)[g+4g+8.g+16{g+8g+8g+8}g+4
     g+4{g+8g+8g+8g+8g+8g+8g+8g+8g+8}
     e4e8.e16{e8e8e8}e4
     e4{e8e8e8e4e4e4}]2
/
v165@33
o3
(41)[g+1^1
     e1^1]6
[g+1^1
 f+1^1]2
e1^1
f+1^1
e1^1
g+1^1

#4
y12v145@33

o4
[r1]4
g+1^2 $DD $00 $C0 $A5 ^1^1^2
/
y11v145@35l16
(50)[o4g+eg+e>c+<g+eg+>c+<g+>c+<g+ec+eg+
ec+ec+g+ec+e>c+<g+eg+eg+ec+
ec+ec+aec+eaeae>c+<ac+e
>c+<aea>c+<aeaeac+eaec+e]6
(51)[g+eg+e>c+<g+eg+>c+<g+>c+<g+ec+eg+
     ec+ec+g+ec+e>c+<g+eg+eg+ec+]1
(52)[f+d+f+d+bf+d+f+bf+bf+>d+<bd+f+
     >d+<bf+b>d+<bf+bf+bd+f+bf+d+f+]1
(51)1
(52)1
(53)[ec+ec+aec+eaeae>c+<ac+e
     >c+<aea>c+<aeaeac+eaec+e]1
(52)1
(53)1
(51)1

#5
y8v145@33
o5
[r1]4
c+1^2 $DD $00 $C0 $A8 ^1^1^2
/
y9v165@33
o3
(61)[>c+1^1
     <a1^1]6
[>c+1^1
 <b1^1]2
a1^1
b1^1
a1^1
>c+1^1


#6
y10v185@31
[o2q7fa+4q7da+4{q7fa+8q7ba+8q7ca+8}q7fa+4
a+4{q7da+8q7aa+8q7ca+8q7fa+4q7ca+4q7ba+4}]11
o2q7fa+4q7da+4{q7fa+8q7ba+8q7ca+8}q7fa+4
v235
@31q7bo2a+8.q78a+8.q7aa+8@38{q7fo4e8q79e8q7ce8q7fe8q79e8q7ce8}
/
(70)[@31q7bo2a+8q78a+8@38q7fo4e8@31q7bo2a+4q78a+8@38q7fo4e8@31q7bo2a+8
a+8q78a+8@38q7fo4e8@31q7bo2a+4q78a+8@38q7fo4e8q7de8]12
/
(70)24

#7 
y9v155@30
o4
(60)[e4e8.e16{e8e8e8}e4
     e4{e8e8e8e8e8e8e8e8e8}
     c+4c+8.c+16{c+8c+8c+8}c+4
     c+4{c+8c+8c+8c+4c+4c+4}]2
[r1]16
/
(80)[y10v115 @23 c8 v150 @23 c8]96
/
(80)96
(80)96