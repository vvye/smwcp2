#SPC
{
	#author "Camerin"
	#comment "Cave theme 3"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 w160 t32

$EF $FF $46 $46
$F1 $03 $32 $01

#0 @11 q7f v220 p40,30 $ED $24 $88
o2[a1]5/
r1^4
o3 $ED $1C $88 p20,40
>a2^4
<a2g2
a2e2
a2g2
a2e4a4
a2g2
a2e2
a2g2
f2e2
>d2<g2
g4g+4a4g4
f2g2
g1
>d2<g2
g2a2
e1
a1
;
#1 @11 q7f v220 p40,30 $ED $24 $88
o3[c1]5/
r1^2
o4 $ED $1C $88 p20,40
g2
[r1]4
c2<b2
>c2<g2
>c2<b2
a2a4g+4
>f2<b2
>c4<b4>c4<b4
a2>c4<b4
>c1
f2<b2
>c2d2
<a2g+2
>e1
;
#2 @11 q7f v220 p40,30 $ED $24 $88
o3[e1]5/
r1^2^4
o4 $ED $1C $88 p20,40
e4
e2d2
e2<b2
>e2d2
e2<b4>e4
e2d2
e2<b2
>e2d2
e1
a2d2
e4d4e4d4
c2d2
e1
a2d2
e2f2
e1
a1
;
#3 @6 q7f v240 $ED $1C $88
r1
o2[a8a16e16g16e16g16g+16a8e16g8a16e16g16]4/
[a8e8g16a16>c16<g16a8e16g16a4]2
a8a16e8a16e16a16g8g16d8g16d16g16
a8a16e8a16e16a16e8g8b8e8
a8a16e8a16e16a16g8g16d8g16d16g16
a8a16e8a16e16a16e8g8a4
a8a16e8a16e16a16g8g16d8g16d16g16
a8a16e8a16e16a16e8g8b8e8
a8a16e8a16e16a16g8g16d8g16d16g16
a8a16e8a16e16a16e4g+4
>d2<g2
e4g+4a4g4
f2g2
>c1
d2<g2
e2f2
e2e2
a1
;
#5 @9 q7f v245 $ED $3D $E6
[r1]4
o4a8a16g16a16e16d16c16d+16d16c16<g16a4/
[r1]2
>a8a16g16a8e8g8f+8e8d8
c16d16e16c16d8<b8g8f+8e8g8
>a8a16g16a8e8g8f+8e8d8
c16d16e16c16d16c16<b16a16g8e8a4
>a8a16g16a8e8g8f+8e8d8
c16d16e16c16d8<b8g8f+8e8g8
>a8a16g16a8e8g8f+8e8d8
c16d16e16c16d16c16<b16a16g+16f+16g+16a16b4
>a4b8>c8<b8a8g8f8
e4d16<b16g+16>d16c4<b8>c8
<b8a8g8a8b8>c8d8d+8
e1
a4b8>c8<b4a8g8
e4c8e8f4e8c8
d8e8f8a8g+4e4
<a1
;
#4 @6 q7f v240
[r1]3
o3[a4>a4g8^16<a16>c16<a16g16e16]2/
[r1]6
@13o4>e4<a4>d8^16c16<b16a16g16b16
>e4<a4g8b8e8g8
>e4<a4>d8^16c16<b16a16g16b16
f16g16a16b16>c8d8e2
[r1]8
;
#7  @29  q7f v220
[r1]2
o3[ @23 g+16 @23 g+16 @29 a+16 @23 g+16]12/
[@10d8@10e16 @29 a8^16@10e16@10e16]36
;
#6  @23  q7f v240
[r1]5/
[ @23 a16]72
[a16]72
[a16]72
[a16]72                

#amk=1
