#samples
{
	#default
	#AMM
	#sky_and_dream
	"CStrings.brr"
     "CChoir.brr"
}


#0 w160 t52 

$EF $FF $5F $5F $F1 $05 $A0 $01
@02 v220 y15 $ED $5B $B3 o4
[b4r4]8
/
(10)[g4r4<b8>e8g8e8
     g4r2g4
     f+4r4d8<a8>d8f+8
     a4f+4d4f+4
     b4r4b8g8b8>e8
     g4e8<b8>e8b8g8e8
     d+8<b8>d+8f+8a8f+8d+8f+8]1
<b8^16r16b8^16r16>c8^16r16<b8^16r16
(10)1
t48
b8^16 t46 r16 t44 b8^16 t42 r16 t40 >c8^16r16d+2^4
t52
r2
e8<b8>e8g8b8g8e8b8
(11)[d+8<b8>d+8f+8]1a8f+8d+8f+8
e8c8e8g8>c8<g8e8g8
(11)1>c8<b8a8b8
e8<b8>e4b8e8g4
d+8<b8>d+4f+8<b8>d+4
c8<g8>c4g8c8e4
f+8d+8f+8a8>c8<b8a8b8
g8f+8e8r8e4g4
b4a8r8a8g4^8
f+8e8d8r8d4f+4
a4a8r8a8f+4^8
c8<g8>c8e8g2
r1
d8<a8>d8f+8a2
a+8g8e8c+8<a8>c+8e8a8
a2
[r2]3

#1 @02 v220 y05
$ED $5B $B3

o4
r8  r2^4^8
[g4r4]6
/
(20)[e4r2^4
     e4r2e4
     d4r2^4
     f+4d4<a4>d+4
     g4r2^4
     >e4r2e8r8
     <b8f+8b8>d+8c8<a8>c8d+8]1
<f+8^16r16f+8^16r16f+8^16r16f+8^16r16
(20)1
f+8^16r16f+8^16r16f+8^16r16b2^4
r2>
(21)[<b8r4^8>]1e8r4^8
(21)1d+8r4^8
c8r4^8e8r4^8
(21)1f+8r4^8
<b8r8g4>e8r8e4
<b8r8b4>d+8r8<b4
g8r8g4>c8r8c4
d+8r4^8f+8r4^8
<b4b8r8b4>e4
g4g8r8g8r4^8
<a4a8r8a4>d4
f+4f+8r8f+8r4^8
<g8e8g8>c8e2
r1
<a8f+8a8>d8f+2
r1
e2
[r2]3

#2 @02 v220 y00
$ED $5B $B3

o4
r1^1
[e4r4]2
[d+4r4]2
/
<
(30)[e8<b8>e8g8]4
(31)[d8<a8>d8f+8]3
(32)[d8<a8>d+8f+8]1
(33)[g8e8g8b8]4
(34)[a8f+8a8>c8d+8c8d+8f+8
     d+8^16r16d+8^16r16d+8^16r16]1d+8^16r16

(30)4
(31)3
(32)1
(33)4
(34)1f+2^4
r2
g4e4g4b4
f+4d+4f+4a4
g4e4g4>c4
<f+4d+4f+4b4
<
[e8g8b8g8]2
[d+8f+8a8f+8]2
[c8e8g8e8]2
f+8a8>c8<a8
f+8a8>c8<b8
(33)4
[f+8d8f+8a8]4
[g8e8g8>c8<]4
[a8f+8a8>d8<]2
a+8g8a+8>c+8<
a8e8a8>c+8
>c+2
[r2]3

#3 @09 v255 y00
$ED $7F $E0

[r1]4
/
o2
(40)[e4e2^8e8
     e4e4e4e4
     d4d2^8d8
     d4d4d4c4
     <b4b2^8b8
     b4b4b4b4
     b4b4>c4c4]1
<b4b4b4b4>

(40)1

[r1]6
[e4]4
[d+4]4
[c4]4
d+4d+4f+4f+4
e4e2^8e8
[e4]4
d4d2^8d8
[d4]4
c4c2^8c8
[c4]4
d4d2^8d8
d+4d+4c+4c+4
c+1
[c+4]4

#7 @00 v140 y10
$ED $6F $B0

o3
{r4e8g8b8>e8g4<g8b8>e8g8
b4<b8>e8g8b8>e4<e8g8b8>e8
g4o3b8>e8g8b8>e4o3g8b8>e8g8
b8g8<b8>d+8g8b8>d+8<b8g8d+8<b8>d+8}
/
[{<g8<b8>e8b8e8g8>e8<g8b8>g8<b8>e8
 b8e8g8>e8<g8b8>g8<b8>e8b8e8g8
 o3a8d8f+8>d8<f+8a8>f+8<a8>d8a8d8f+8
 >d8<f+8a8>f+8<a8>d8a8d8f+8>c8<f+8a8
 o3g8<b8>e8b8e8g8>e8<g8b8>g8<b8>e8
 b8e8g8>e8<g8b8>g8<b8>e8b8e8g8
 o3b8d+8f+8>d+8<f+8b8>f+8c8d+8a8d+8f+8
 f+8<f+8b8>d+8<d+8f+8b8<b8>d+8f+8<f+8b8}]2

[r1]5

o4
{e8<g8b8>g8<b8>e8b8e8g8>e8<g8b8
d+8<f+8b8>f+8<b8>d+8b8d+8f+8>d+8<f+8b8
c8<e8g8>e8<g8>c8g8c8e8>c8<e8g8
d+8<f+8a8>f+8<a8>c8a8c8d+8>c8<d+8f+8
<g8<b8>e8b8e8g8>e8<g8b8>g8<b8>e8
b8e8g8>e8<g8b8>g8<b8>e8b8e8g8
o3a8d8f+8>d8<f+8a8>f+8<a8>d8a8d8f+8
>d8<f+8a8>f+8<a8>d8a8d8f+8>d8<f+8a8}
r1
{c8<e8g8>c8<e8g8>c8<e8g8>c8<e8g8}
r1
{a+8e8g8e8<a+8>c+8<a8c+8e8c+8<e8a8}
r1
{>a8c+8e8>c+8<e8a8>e8<a8>c+8a8c+8e8}

#5 ("CStrings.brr", $03) h-8 v130 y18
$ED $19 $A0

o4
e1^1^1
d+1
/
(60)[e1^1
     d1^2^4
     c4
     <b1^1
     b2>c2]1
<b2f+2>

(60)1

r1^1

@0 ("CChoir.brr", $06) h0
$ED $1C $A0
v155
y10

d+1
<a+1
b1>
d1
d+1
<a+1
b1>
d2f2

("CStrings.brr", $03) h-8
$ED $19 $A0
v130
y18

e1^1
d1^1
c1^1
d1
e2c+1^1^2

#6 ("CStrings.brr", $03) h-8 v130 y18
$ED $19 $A0

o3
b1^1^1^1
/
(70)[b1^1
     a1^1
     g1^1
     f+2a2]1
f+2d+2

(70)1

r1^1

@0 ("CChoir.brr", $06) h0
$ED $1C $A0
v155
y10

a+1
f1
f+1
a+2b2
a+1
f1
f+1
a+2>d2

("CStrings.brr", $03) h-8
$ED $19 $A0
v130
y18

<b1^1
a1^1
g1^1
a1
a+2a1^1^2

#4 ("CStrings.brr", $03) h-8 v130 y18
$ED $19 $A0

o3
r1^1
g1^1
/
(80)[g1^1
     f+1^1
     e1^1
     d+1]1
d+2<b2>

(80)1

r1^1

@0 ("CChoir.brr", $06) h0
$ED $1C $A0
v155
y10

f+1
d1
d+1
f1
f+1
d1
d+1
f2b2>

("CStrings.brr", $03) h-8
$ED $19 $A0
v130
y18

<g1^1
f+1^1
e1^1
f+1
g2e1^1^2                

#amk=1
