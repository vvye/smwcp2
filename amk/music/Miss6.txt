#amk 2

#pad $01E5

#option TempoImmunity
#option NoLoop

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 $F4 $02
@3 t74 o5v255 e32d32e32v0 t90 ^4^16^32@9 $ED $7F $F3 t28 o3v255 c+16>c+32d32e16d32c+32<b16e32a32b16e16a2^16^32


#1
r2@9 $ED $7F $F3 o3v255 g+32g+32g+16a32g+32f+16<b32>e32f+16<b16>e2^16^32


#2
r4^8^16 @22 o3v255c16 @22  v128 c16@22  v157 c16@22  v186 c16@22 v255 c16@22  v157 c16@22 v255 c16@22  v128 c32@22  v157 c32@22 v255 c2^16^32

#3 $EE $FF v220
r4^8^16 $F3 $20 $04 $ED $76 $E0 o4 c8d+16c16<a+16d+16f16g16g+4 $E8 $0C $50 ^4^16^32                