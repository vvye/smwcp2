;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;SMB2/3 Vertical Level Mode "Generator", by Magus (a. k. a. M79X.EXE)
;
;Uses extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    PRINT "INIT ",pc              
                    PRINT "MAIN ",pc                                    
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITE_CODE_START:   STZ $1411

CHECK_LEFT:	    LDA $94
		    CMP #$0A
		    BCS CHECK_RIGHT
		    LDA #$E7
		    STA $94

CHECK_RIGHT:	    CMP #$E8
		    BCC RETURN
		    LDA #$0B
		    STA $94

RETURN:              JSR SPRITE_LOOP

		    RTS        

SPRITE_LOOP:	    PHX

		    LDX #$00

LOOP_START:	    LDA $E4,x
		    CMP #$08
		    BCS SPRITE_RIGHT
		    LDA #$F7
		    STA $E4,x
		    BRA RETURN2

SPRITE_RIGHT:	    CMP #$F8
		    BCC RETURN2
		    LDA #$09
		    STA $E4,x

RETURN2:		    INX
		    CPX #$13
		    BCC LOOP_START

		    PLX

		    RTS

