;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 16x32 Marine Pop -                                              ;
;           Small Marine Pop sprite that uses Mario's collision.  ;
;           Tries to avoid collision errors.                      ;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


!TEMP = $09

;!TORPEDO SPRITE NUMBER!
!PROJECTILE = $A8

;generated xspeed
!GENSPEEDX = $40

;gen offsets
!GENPOSX	=	$0004
!GENPOSY	=	$000E

!IN_FLIGHT = $1510

PRINT "INIT ",pc
;LDA $94
;STA $E4,x
;LDA $95
;STA $14E0,x
;LDA $96
;STA $D8,x
;LDA $97
;STA $14D4,x
stz !IN_FLIGHT,x
RTL

PRINT "MAIN ",pc
PHB
PHK
PLB
JSR SPRITE_ROUTINE
PLB
RTL

SPRITE_ROUTINE:
lda !IN_FLIGHT,x
bne FLYING
    jsr SUB_GFX_PRE_FLIGHT
    jsl $01A7DC
    bcc NOTTOUCHINGMARIO
        LDA $94
        STA $E4,x
        LDA $95
        STA $14E0,x
        LDA $96
        STA $D8,x
        LDA $97
        STA $14D4,x
        lda #$01
        sta !IN_FLIGHT,x
    NOTTOUCHINGMARIO:
    rts
FLYING:
JSR SUB_GFX
LDA #$01		;Load 1 into the accumulator
STA $1404		;Flag for free will scrolling
LDA #$1C
STA $78

LDA $9D
BNE RETURN_2

LDA $1DF9		;block swimming sound
CMP #$0E
BNE NOSWIMSOUND
STZ $1DF9		;zero sound

NOSWIMSOUND:
LDA $1DFA		;block jumping sound
CMP #$01
BNE NOJUMPSOUND
STZ $1DFA		;zero sound

NOJUMPSOUND:
STZ $148F		;mario never caries anything
STZ $13E8		;disable cape spin
STZ $73

LDA $1DF9		;block cape spin sound
CMP #$0F
BNE NOCAPESOUND
STZ $1DF9		;zero sound

NOCAPESOUND:
LDA $1DF9		;block spin jump sound
CMP #$02
BNE NOSPINSOUND
STZ $1DF9		;zero sound

NOSPINSOUND:
PHY
LDY #$00		;loop index
EXTENDEDLOOP:
LDA $170B,y
CMP #$05		;test mario fireball
BNE NOTFIREBALL
LDA #$00		;zero slot
STA $170B,y
NOTFIREBALL:
INY			;next ex sprite
CPY #$0A		;10 sprites
BNE EXTENDEDLOOP	;loop
PLY

LDA #$01
STA $76
LDA #$20
STA $13E0

LDA $15
AND #$01
BEQ NOT_RIGHT

LDA #$15
STA $7B
LDA $77
AND #$01
BNE NOT_HORZ
LDA $77
AND #$10
BNE NOT_HORZ
BRA CHEK_VERT

NOT_RIGHT:
LDA $15
AND #$02
BEQ NOT_HORZ
BRA MOVE

RETURN_2:
BRA RETURN

MOVE:
LDA #$EC
STA $7B
LDA $77
AND #$02
BNE NOT_HORZ
LDA $77
AND #$10
BNE NOT_HORZ
BRA CHEK_VERT

NOT_HORZ:
STZ $7B

CHEK_VERT:
LDA $15
AND #$04
BEQ NOT_DOWN

LDA #$15
STA $7D
BRA MOV_MAR

NOT_DOWN:
LDA $15
AND #$08
BEQ NOT_VERT

LDA #$EC
STA $7D
BRA MOV_MAR

NOT_VERT:
STZ $7D

MOV_MAR:
LDA $94
STA $E4,x
LDA $95
STA $14E0,x
LDA $96
CLC
ADC #$01
STA $D8,x
LDA $97
ADC #$00
STA $14D4,x

LDA $13
AND #$03
BEQ CNTU_FIRE
INC $C2,x
LDA $C2,x
CMP #$03
BNE CNTU_FIRE
STZ $C2,x

CNTU_FIRE:
LDA $16
AND #$40
BNE SHOOT
RETURN:
RTS

SHOOT:
LDA $1558,x
BNE RETURN
PHX
PHY
LDY #$00
LDX #$0B
DUPELOOP:
LDA $7FAB9E,x
CMP #!PROJECTILE  ;Assuming you are using TRASM
BNE NOTDUPE
LDA $14C8,x
CMP #$08
BNE NOTDUPE
INY
CPY #$03
BCS DONTSPAWNTORP
NOTDUPE:
DEX
BPL DUPELOOP
PLY
PLX

LDA #$10
STA $1558,x

;generate projectileGenCust
	JSL $02A9DE		;grab free sprite slot low priority
	BMI NOFIRE		;RETURN if none left before anything else...

	LDA #$06		;make fireball sound
	STA $1DFC

	JSR SETUPCUST		;run sprite generation routine
	NOFIRE:
	BRA RETURN

SETUPCUST:
	LDA #$08
	STA $14C8,y		;normal status, run init routine
	LDA #!PROJECTILE

	PHX
	TYX
	STA $7FAB9E,x		;into sprite type table for custom sprites
	PLX
	
;set positions accordingly		
;restore gen sprite index

	LDA $14E0,x		;create base 16bit pos
	XBA
	LDA $E4,x

FACINGRIGHT:
	LDA $14E0,x		;base xpos
	XBA
	LDA $E4,x
	REP #$20
	CLC
	ADC.w #!GENPOSX		;add
	SEP #$20
	STA $00E4,y		;store
	XBA
	STA $14E0,y
	
	LDA $14D4,x		;base ypos
	XBA
	LDA $D8,x
	REP #$20
	CLC
	ADC.w #!GENPOSY		;add
	SEP #$20
	STA $00D8,y		;store
	XBA
	STA $14D4,y

	TYX			;new sprite slot into X
	JSL $07F7D2		;create sprite
	JSL $0187A7
        LDA #$88		;set as custom sprite
        STA $7FAB10,X

;set projectile speed according to Mario
	LDX $15E9		;restore sprite index

	LDA #!GENSPEEDX
	STA $00B6,y		;set xspeed

PHX
PHY
DONTSPAWNTORP:
PLY
PLX
	RTS			;RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!EXTRA_BITS = $7FAB10
TILES:
    db $EC,$EE

XOFF:
    db $F8,$08

YOFF:
    db $10,$10

PROPERTIES:
    db $00,$00

;TILETOGET: 
;	db $CC,$CC,$CC,$CC
;TILETOGET2:
;	db $Ec,$CE,$EE,$CE
;TILETOGET3:
;	db $EC,$BC,$EE,$EC
;TILETOGET4:
;	db $AA,$AC,$EC,$AC

    SUB_GFX_PRE_FLIGHT:
        JSR GET_DRAW_INFO
        lda $15F6,x
        sta $06
        phx
        ldx #$01
        GFX_LOOP1:
            lda XOFF,x
            clc
            adc $00
            sta $0300,y
            lda YOFF,x
            clc
            adc $01
            sec
            sbc #$10
            sta $0301,y
            lda TILES,x
            sta $0302,y
            LDA $06
            ORA $64
            STA $0303,y
            iny
            iny
            iny
            iny
            dex
            bpl GFX_LOOP1
        plx
        ldy #$02
        lda #$01
        jsl $01B7B3
        rts

	SUB_GFX:
	JSR GET_DRAW_INFO
	LDY #$0C
    lda $15F6,x
    sta $06
    phx
    ldx #$01
    GFX_LOOP2:
        stz $07
        lda XOFF,x
        clc
        adc $00
        sta $0200,y
        lda $19
        beq NOTBIG
            lda $0200,y
            inc
            sta $0200,y
        NOTBIG:
        lda $0200,y
        cmp #$F8
        bcc NOTOFFSCREEN
        OFFSCREEN:
            lda #$01
            sta $07
        NOTOFFSCREEN:
        lda YOFF,x
        clc
        adc $01
        sta $0201,y
        lda TILES,x
        sta $0202,y
        LDA $06
        ORA $64
        STA $0203,y
        phy
        tya
        lsr
        lsr
        tay
        phx
        ldx $15E9
        lda #$02
        ora $15A0,x
        ora $07
        sta $0420,y
        plx
        ply
        iny
        iny
        iny
        iny
        dex
        bpl GFX_LOOP2
    plx
    ;lda #$01
    ;ldy #$02
    ;jsl $01B7B3
    rts
	;LDA $157C,x
	;STA $02
	;LDA !EXTRA_BITS,x
	;AND #$04
	;BNE OTHER_TILE

	;LDA $00
	;STA $0200,y
	;LDA $01
	;STA $0201,y

	;PHY
	;LDY $19
	;LDA TILETOGET,y
	;BRA SET1

	;OTHER_TILE
	;LDA $00
	;CLC
	;ADC #$10
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET4,y
	;SET1
	;PLY
	;STA $0202,y
	;LDA $15F6,x
	;ORA $64
	;STA $0203,y

	;PHY
	;TYA ;
	;LSR ;
	;LSR ; Set to 16x16 tile
	;TAY ;
	;LDA #$02 ;
	;ORA $15A0,x ; horizontal OFFSCREEN flag
	;STA $0420,y ;
	;PLY
	;INY
	;INY
	;INY
	;INY


	;LDA !EXTRA_BITS,x
	;AND #$04
	;BNE OTHER_TILE2

	;LDA $00
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET2,y
	;BRA SET2

	;OTHER_TILE2
	;LDA $00
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET3,y

	;SET2
	;PLY
	;STA $0202,y
	;LDA $15F6,x
	;ORA $64
	;STA $0203,y

	;PHY
	;TYA ;
	;LSR ;
	;LSR ; Set to 16x16 tile
	;TAY ;
	;LDA #$02 ;
	;ORA $15A0,x ; horizontal OFFSCREEN flag
	;STA $0420,y ;
	;PLY
	;RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GET_DRAW_INFO
; This is a helper for the graphics routine.  It sets off screen flags, and sets up
; variables.  It will RETURN with the following:
;
;		Y = index to sprite OAM ($300)
;		$00 = sprite x position relative to screen boarder
;		$01 = sprite y position relative to screen boarder  
;
; It is adapted from the subroutine at $03B760
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPR_T1:              db $0C,$1C
SPR_T2:              db $01,$02

GET_DRAW_INFO:       STZ $186C,x             ; reset sprite OFFSCREEN flag, vertical
                    STZ $15A0,x             ; reset sprite OFFSCREEN flag, horizontal
                    LDA $E4,x               ; \
                    CMP $1A                 ;  | set horizontal OFFSCREEN if necessary
                    LDA $14E0,x             ;  |
                    SBC $1B                 ;  |
                    BEQ ON_SCREEN_X         ;  |
                    INC $15A0,x             ; /

ON_SCREEN_X:         LDA $14E0,x             ; \
                    XBA                     ;  |
                    LDA $E4,x               ;  |
                    REP #$20                ;  |
                    SEC                     ;  |
                    SBC $1A                 ;  | mark sprite INVALID if far enough off screen
                    CLC                     ;  |
                    ADC.w #$0040            ;  |
                    CMP.w #$0180            ;  |
                    SEP #$20                ;  |
                    ROL A                   ;  |
                    AND #$01                ;  |
                    STA $15C4,x             ; / 
                    BNE INVALID             ; 
                    
                    LDY #$00                ; \ set up loop:
                    LDA $1662,x             ;  | 
                    AND #$20                ;  | if not smushed (1662 & 0x20), go through loop twice
                    BEQ ON_SCREEN_LOOP      ;  | else, go through loop once
                    INY                     ; / 
ON_SCREEN_LOOP:      LDA $D8,x               ; \ 
                    CLC                     ;  | set vertical OFFSCREEN if necessary
                    ADC SPR_T1,y            ;  |
                    PHP                     ;  |
                    CMP $1C                 ;  | (vert screen boundry)
                    ROL $00                 ;  |
                    PLP                     ;  |
                    LDA $14D4,x             ;  | 
                    ADC #$00                ;  |
                    LSR $00                 ;  |
                    SBC $1D                 ;  |
                    BEQ ON_SCREEN_Y         ;  |
                    LDA $186C,x             ;  | (vert OFFSCREEN)
                    ORA SPR_T2,y            ;  |
                    STA $186C,x             ;  |
ON_SCREEN_Y:         DEY                     ;  |
                    BPL ON_SCREEN_LOOP      ; /

                    LDY $15EA,x             ; get offset to sprite OAM
                    LDA $E4,x               ; \ 
                    SEC                     ;  | 
                    SBC $1A                 ;  | $00 = sprite x position relative to screen boarder
                    STA $00                 ; / 
                    LDA $D8,x               ; \ 
                    SEC                     ;  | 
                    SBC $1C                 ;  | $01 = sprite y position relative to screen boarder
                    STA $01                 ; / 
                    RTS                     ; RETURN

INVALID:             PLA                     ; \ RETURN from *main gfx routine* subroutine...
                    PLA                     ;  |    ...(not just this subroutine)
                    RTS                     ; /

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, RETURN
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process OFFSCREEN" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; RETURN

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process OFFSCREEN" flag is set, RETURN
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00				;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
					LDA $94					;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
					SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
					SBC $E4,x				;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
					STA $0F					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
					LDA $95					;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
					SBC $14E0,x				;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
					BPL SPR_L16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
					INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
SPR_L16:				RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642
