;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Red Elite Koopa
; by yoshicookiezeus
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

incsrc subroutinedefs_xkas.asm

			!RAM_FrameCounter	= $13
			!RAM_FrameCounterB	= $14
			!RAM_ScreenBndryXLo	= $1A
			!RAM_ScreenBndryYLo	= $1C
			!RAM_MarioDirection	= $76
			!RAM_MarioSpeedX	= $7B
			!RAM_MarioSpeedY	= $7D
			!RAM_MarioXPos		= $94
			!RAM_MarioXPosHi	= $95
			!RAM_MarioYPos		= $96
			!RAM_SpritesLocked	= $9D
			!RAM_SpriteNum		= $9E
			!RAM_SpriteSpeedY	= $AA
			!RAM_SpriteSpeedX	= $B6
			!RAM_SpriteState	= $C2
			!RAM_SpriteYLo		= $D8
			!RAM_SpriteXLo		= $E4
			!OAM_DispX		= $0300
			!OAM_DispY		= $0301
			!OAM_Tile		= $0302
			!OAM_Prop		= $0303
			!OAM_Tile2DispX		= $0304
			!OAM_Tile2DispY		= $0305
			!OAM_Tile2		= $0306
			!OAM_Tile2Prop		= $0307
			!OAM_TileSize		= $0460
			!RAM_RandomByte1	= $148D
			!RAM_RandomByte2	= $148E
			!RAM_KickImgTimer	= $149A
			!RAM_SpriteYHi		= $14D4
			!RAM_SpriteXHi		= $14E0
			!RAM_Reznor1Dead	= $1520
			!RAM_Reznor2Dead	= $1521
			!RAM_Reznor3Dead	= $1522
			!RAM_Reznor4Dead	= $1523
			!RAM_DisableInter	= $154C
			!RAM_SpriteDir		= $157C
			!RAM_SprObjStatus	= $1588
			!RAM_OffscreenHorz	= $15A0
			!RAM_SprOAMIndex	= $15EA
			!RAM_SpritePal		= $15F6
			!RAM_Tweaker1662	= $1662
			!RAM_ExSpriteNum	= $170B
			!RAM_ExSpriteYLo	= $1715
			!RAM_ExSpriteXLo	= $171F
			!RAM_ExSpriteYHi	= $1729
			!RAM_ExSpriteXHi	= $1733
			!RAM_ExSprSpeedY	= $173D
			!RAM_ExSprSpeedX	= $1747
			!RAM_OffscreenVert	= $186C
			!RAM_OnYoshi		= $187A

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "INIT ",pc
			JSR SUB_HORZ_POS
			TYA
			STA !RAM_SpriteDir,x
			LDA #$40			;\
			STA $1540,x			;/ set time before attacking
			RTL				; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			print "MAIN ",pc
			PHB
			PHK
			PLB
			JSR MainCode
			PLB
			RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MainCode:		JSR SpriteGraphics
			LDA $14C8,x			;\ if sprite status not normal,
			CMP #$08			; |
			BNE Return1			;/ branch
			LDA !RAM_SpritesLocked		;\ if sprites locked,
			BNE Return0			;/ branch
			JSR SUB_OFF_SCREEN_X0
			LDA $151C,x			;\ call pointer for current action
			JSL $0086DF			;/

Pointers:		dw Walk
			dw Attack
			dw Hurt
			dw Dead

Return0:		RTS				; return

Return1:		INC $1540,x
			RTS

HurtInState:		db $01,$01,$00
!Hitpoints		= $03

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedWalk:		db $08,$F8

Walk:			LDY !RAM_SpriteDir,x		;\ set sprite speed depending on direction
			LDA XSpeedWalk,y		; |
			STA !RAM_SpriteSpeedX,x		;/
			JSL $01802A			; update sprite position
			JSL $019138			; interact with objects
			JSL $018032			; interact with sprites
			LDA !RAM_SprObjStatus,x		;\ if sprite isn't touching wall,
			AND #$03			; |
			BEQ DontFlipWalk		;/ branch
			LDA !RAM_SpriteDir,x		;\ flip sprite direction
			EOR #$01			; |
			STA !RAM_SpriteDir,x		;/
DontFlipWalk:		LDA !RAM_SprObjStatus,x		;\ if sprite is on ground,
			AND #$04			; |
			BNE OnGroundWalk		;/
			INC $1540,x
OnGroundWalk:		LDA $1540,x			;\ if not yet time to attack,
			BNE StillState0			;/ branch
			LDA #$01			;\ set new sprite state (attacking)
			STA $151C,x			;/
			LDA #$50			;\ set time to attack
			STA $1540,x			;/
			JSR SUB_HORZ_POS
			TYA
			STA !RAM_SpriteDir,x
StillState0:		JSR MarioInteract			
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Attack:			JSR SUB_HORZ_POS
			TYA
			STA !RAM_SpriteDir,x
			STZ !RAM_SpriteSpeedX,x
			JSL $01802A			; update sprite position
			JSL $019138			; interact with objects
			JSL $018032			; interact with sprites
			LDA $1540,x
			CMP #$40
			BCS NoFire
			AND #$0F
			CMP #$08
			BNE NoFire

			LDY #$07
CheckFire:		LDA $170B,y
			BEQ ContinueFire
			DEY
			BPL CheckFire
			RTS
ContinueFire:		LDA #$02
			STA !RAM_ExSpriteNum,y
			LDA !RAM_SpriteXLo,x			;\ set sprite x position
			CLC
			ADC #$04
			STA !RAM_ExSpriteXLo,y			; |
			LDA !RAM_SpriteXHi,x			; |
			ADC #$00
			STA !RAM_ExSpriteXHi,y			;/
			LDA !RAM_SpriteYLo,x			;\
			STA !RAM_ExSpriteYLo,y			; |
			LDA !RAM_SpriteYHi,x			; |
			STA !RAM_ExSpriteYHi,y			;/
			LDA #$20
			JSR CODE_01BF6A
			LDA $00
			STA !RAM_ExSprSpeedY,y
			LDA $01
			STA !RAM_ExSprSpeedX,y
			LDA #$06
			STA $1DFC
			RTS

NoFire:			LDA $1540,x			;\ if not yet time to stop attack,
			BNE StillState1			;/ branch
			LDA #$00			;\ set new sprite state (walking)
			STA $151C,x			;/
			LDA #$40			;\ set time to attack
			STA $1540,x			;/
StillState1:		JSR MarioInteract			
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Hurt:			STZ !RAM_SpriteSpeedX,x		; clear sprite speed
			JSL $01802A			; update sprite position
			JSL $019138			; interact with objects
			JSL $018032			; interact with sprites
			LDA $1540,x			; if not yet time to stop being hurt,
			BNE StillState2			; branch
			LDA #$00			;\ set new sprite state (walking)
			STA $151C,x			;/
			LDA #$40			;\ set time to attack
			STA $1540,x			;/
StillState2:		JSR MarioInteract
			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Dead:			RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Mario interaction routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MarioInteract:		JSL $01A7DC			;\ interact with Mario
			BCC Return03CEF1		;/ if no contact, branch
			LDA $1490			;\ if star timer isn't zero,
			BNE KillSprite			;/ branch
			LDA $154C,x
			BNE Return03CEF1
			LDA !RAM_MarioSpeedY		;\ if Mario is moving upwards,
			CMP #$10			; |
			BMI CODE_03CEED			;/  branch
			JSL $01AB99			; display contact graphic
			JSL $01AA33			; boost Mario upwards
			LDA #$02			;\ play sound effect
			STA $1DF9			;/
			LDA #$08
			STA $154C,x
			LDY $151C,x			;\ if current state is set to be invincible,
			LDA HurtInState,y		; |
			BEQ Return03CEEC		;/ branch
			INC $1534,x			; increase hitpoint counter
			LDA $1534,x			;\ if sprite hasn't been hit enough times,
			CMP #!Hitpoints			; |
			BNE CODE_03CEDB			;/ branch
KillSprite:		LDA #$02			;\ kill sprite
			STA $14C8,x			;/
			LDA #$E0
			STA !RAM_SpriteSpeedY,x
			LDA #$13			;\ play sound effect
			STA $1DF9			;/
			LDA #$03			;\ set new action (dead)
			STA $151C,x			;/
			LDA #$04			;\ give Mario 400 points
			JSL $02ACE5			;/
			RTS
CODE_03CEDB:		LDA #$02			;\ set action (stomped)
			STA $151C,x			;/
			LDA #$13			;\ play sound effect
			STA $1DF9			;/
			LDA #$40			;\ set time to stay in hurt state
			STA $1540,x			;/
Return03CEEC:		RTS				; return

CODE_03CEED:		JSL $00F5B7			; hurt Mario
Return03CEF1:		RTS				; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Tilemap:		db $08,$28			; walking frame 1, right
			db $06,$26			; walking frame 2, right
			db $20,$40			; walking frame 1, left
			db $22,$42			; walking frame 2, left
			db $4B,$6B			; attacking frame, right
			db $24,$44			; attacking frame, left
			db $0C				; hurt frame
			db $64				; dead frame

OffsetY:		db $F0,$00
			db $F0,$00
			db $F0,$00
			db $F0,$00
			db $F0,$00
			db $F0,$00
			db $00
			db $00

TilesToDraw:		db $01,$01,$00,$00		; walking, attacking, hurt, dead

StartOffset:		db $00,$08,$0C,$0D		; walking, attacking, hurt, dead

FrameDeterminatorBits:	db $02,$00,$00,$00		; walking, attacking, hurt, dead

DirectionOffset:	db $04,$02,$00,$00
			

SpriteGraphics:		JSL !GetDrawInfo

			STZ $02
			LDA !RAM_SpriteDir,x
			BEQ Right
			LDA $151C,x
			PHX
			TAX
			LDA DirectionOffset,x
			STA $02
			PLX

Right:			PHX
			LDA $151C,x
			TAX
			LDA FrameDeterminatorBits,x
			STA $03

			LDA StartOffset,x
			STA $04

			PLX
			LDA $1540,x
			LSR
			LSR
			AND $03
			CLC
			ADC $02
			CLC
			ADC $04
			STA $05
			PHX
			LDA $151C,x
			TAX
			LDA TilesToDraw,x
			STA $06
			TAX



Loop_Start:		PHX				; preserve loop counter

			LDX $05

			LDA $00				;\ set tile x position
			STA !OAM_DispX,y		;/

			LDA $01				;\ set tile y position
			CLC				; |
			ADC OffsetY,x			; |
			STA !OAM_DispY,y		;/

			LDA Tilemap,x			;\ set tile number
			STA !OAM_Tile,y			;/

			LDX $15E9			; |
			LDA $15F6,x			; |
			ORA $64				; |
			STA !OAM_Prop,y			;/

			INY				;\ increase OAM index by four, so that the next tile can be drawn
			INY				; |
			INY				; |
			INY				;/

			INC $05
			PLX				; retrieve loop counter
			DEX				; decrease loop counter
			BPL Loop_Start			; if more tiles left to draw, branch

			PLX				; retrieve sprite index

			LDY #$02
			LDA $06
			JSL $01B7B3
			
			RTS				; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; aiming routine
; input: accumulator should be set to total speed (x+y)
; output: $00 = y speed, $01 = x speed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CODE_01BF6A:		STA $01
			PHX					;\ preserve sprite indexes of Magikoopa and magic
			PHY					;/
			JSR CODE_01AD42				; $0E = vertical distance to Mario
			STY $02					; $02 = vertical direction to Mario
			LDA $0E					;\ $0C = vertical distance to Mario, positive
			BPL CODE_01BF7C				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
CODE_01BF7C:		STA $0C					;/
			JSR SUB_HORZ_POS			; $0F = horizontal distance to Mario
			STY $03					; $03 = horizontal direction to Mario
			LDA $0F					;\ $0D = horizontal distance to Mario, positive
			BPL CODE_01BF8C				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
CODE_01BF8C:		STA $0D					;/
			LDY #$00
			LDA $0D					;\ if vertical distance less than horizontal distance,
			CMP $0C					; |
			BCS CODE_01BF9F				;/ branch
			INY					; set y register
			PHA					;\ switch $0C and $0D
			LDA $0C					; |
			STA $0D					; |
			PLA					; |
			STA $0C					;/
CODE_01BF9F:		LDA #$00				;\ zero out $00 and $0B
			STA $0B					; | ...what's wrong with STZ?
			STA $00					;/
			LDX $01					;\ divide $0C by $0D?
CODE_01BFA7:		LDA $0B					; |\ if $0C + loop counter is less than $0D,
			CLC					; | |
			ADC $0C					; | |
			CMP $0D					; | |
			BCC CODE_01BFB4				; |/ branch
			SBC $0D					; | else, subtract $0D
			INC $00					; | and increase $00
CODE_01BFB4:		STA $0B					; |
			DEX					; |\ if still cycles left to run,
			BNE CODE_01BFA7				;/ / go to start of loop
			TYA					;\ if $0C and $0D was not switched,
			BEQ CODE_01BFC6				;/ branch
			LDA $00					;\ else, switch $00 and $01
			PHA					; |
			LDA $01					; |
			STA $00					; |
			PLA					; |
			STA $01					;/
CODE_01BFC6:		LDA $00					;\ if horizontal distance was inverted,
			LDY $02					; | invert $00
			BEQ CODE_01BFD3				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
			STA $00					;/
CODE_01BFD3:		LDA $01					;\ if vertical distance was inverted,
			LDY $03					; | invert $01
			BEQ CODE_01BFE0				; |
			EOR #$FF				; |
			CLC					; |
			ADC #$01				; |
			STA $01					;/
CODE_01BFE0:		PLY					;\ retrieve Magikoopa and magic sprite indexes
			PLX					;/
			RTS					; return



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			
SPR_T12:		db $40,$B0
SPR_T13:		db $01,$FF
SPR_T14:		db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
			db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:		db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
			db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:	LDA #$02                ; \ entry point of routine determines value of $03
			BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:	LDA #$04                ;  | 
			BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:	LDA #$06                ;  |
			BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:	LDA #$08                ;  |
			BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:	LDA #$0A                ;  |
			BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:	LDA #$0C                ;  |
			BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:	LDA #$0E                ;  |
STORE_03:		STA $03			;  |            
			BRA START_SUB		;  |
SUB_OFF_SCREEN_X0:	STZ $03			; /

START_SUB:		JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
			BEQ RETURN_35           ; /
			LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
			AND #$01                ; |
			BNE VERTICAL_LEVEL      ; /     
			LDA $D8,x               ; \
			CLC			; | 
			ADC #$50                ; | if the sprite has gone off the bottom of the level...
			LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
			ADC #$00                ; | 
			CMP #$02                ; | 
			BPL ERASE_SPRITE        ; /    ...erase the sprite
			LDA $167A,x             ; \ if "process offscreen" flag is set, return
			AND #$04                ; |
			BNE RETURN_35           ; /
			LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
			AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
			ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
			STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
			TAY			;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
			LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
			CLC			;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
			ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
			ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
			CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
			PHP			;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
			LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
			LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
			ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
			PLP			;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
			SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
			STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
			LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
			BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
			EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
			STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:		LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
			BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:		LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
			CMP #$08                ; |
			BCC KILL_SPRITE         ; /    
			LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
			CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
			BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
			LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
			STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:		STZ $14C8,x             ; erase sprite
RETURN_35:		RTS			 ; return

VERTICAL_LEVEL:		LDA $167A,x             ; \ if "process offscreen" flag is set, return
			AND #$04                ; |
			BNE RETURN_35           ; /
			LDA $13                 ; \
			LSR A                   ; | 
			BCS RETURN_35           ; /
			LDA $E4,x               ; \ 
			CMP #$00                ;  | if the sprite has gone off the side of the level...
			LDA $14E0,x             ;  |
			SBC #$00                ;  |
			CMP #$02                ;  |
			BCS ERASE_SPRITE        ; /  ...erase the sprite
			LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
			LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
			AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
			STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
			TAY			;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
			LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
			CLC			;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
			ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
			ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
			CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
			PHP			;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
			LDA $001D               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
			LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
			ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
			PLP			;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
			SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
			STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
			LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
			BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
			EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
			STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:		LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
			BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
			BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:	LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
			ORA $186C,x             ; |  
			RTS			; / return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - shared
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

			;org $03B817        

SUB_HORZ_POS:		LDY #$00		;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
			LDA $94			;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
			SEC			;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
			SBC $E4,x		;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
			STA $0F			;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
			LDA $95			;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
			SBC $14E0,x		;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
			BPL LABEL16		;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
			INY			;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:		RTS			;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642

CODE_01AD42:		LDY #$00
			LDA $D3
			CLC					;\ make subroutine use position of Mario's lower half instead of the upper one
			ADC #$10				;/ this wasn't in the original routine
			SEC
			SBC !RAM_SpriteYLo,x
			STA $0E
			LDA $D4
			SBC !RAM_SpriteYHi,x
			BPL Return01AD53
			INY
Return01AD53:		RTS					; return