incsrc subroutinedefs_xkas.asm

!Tile = $A6		;16x16 tile of bullet bill image (SP2)
!Turnblock = $011E	;Which map16 tile is a turn block? Turn blocks shatter when blasted by this projectile

print "INIT ",pc
		RTL

print "MAIN ",pc
		PHB
		PHK
		PLB
		JSR Main
		PLB
		RTL

Speedtbl:	db $40^$FF,$40

Main:		JSR Graphics
		LDA $9D
		BNE Return

		JSL Offscreenhandle

		STZ $AA,x
		LDY $157C,x
		LDA Speedtbl,y
		STA $B6,x
		JSL $018022

		JSR Interact
		JSR CheckWall

		JSL $019138
		LDA $1862
		XBA
		LDA $1860
		REP #$20
		CMP #!Turnblock
		SEP #$20
		BNE Return

		STZ $14C8,x
		PHB			;preserve current bank
		LDA #$02		;push 02
		PHA
		PLB			;bank = 02
		LDA #$00		;default shatter
		JSL $028663		;shatter block
		PLB			;restore bank

		LDA #$02		;block to generate
		STA $9C
		JSL $00BEB0		;generate

		JSR Show_Smoke		;smoke

Return:		RTS

CheckWall:	LDA $1588,x
		AND #$03
		BEQ Return

		LDA $157C,x
		BNE +

		LDA $D8,x
		CLC
		ADC #$08
		AND #$F0
		STA $98
		LDA $14D4,x
		STA $99
		LDA $E4,x

		STA $9A
		LDA $14E0,x
		STA $9B
		BRA Cont

+		LDA $D8,x
		CLC
		ADC #$08
		AND #$F0
		STA $98
		LDA $14D4,x
		STA $99	

		LDA $E4,x
		CLC
		ADC #$0B
		AND #$F0
		STA $9A
		LDA $14E0,x
		STA $9B

Cont:		PHK			;push current program bank
		PEA.w ReturnAdd-$01	;push return address -1
		PHB			;preserve bank
		LDA #$01		;bank 01
		PHA		
		PLB			;new data bank
		PEA $8027		;push return address - 1 containing PLB + RTL
		JML $01999E		;long jump to local routine

ReturnAdd:	STZ $14C8,x		;erase sprite
		LDA #$25
		STA $1DFC
		JSR Show_Smoke		;smoke
		RTS

Show_Smoke:	LDY #$03		; \ find a free slot to display effect
.FINDFREE	LDA $17C0,y		;  |
		BEQ .FOUNDONE		;  |
		DEY			;  |
		BPL .FINDFREE		;  |
		RTS			; / return if no slots open

.FOUNDONE	LDA #$01		; \ set effect to smoke
		STA $17C0,y		; /

		LDA $D8,x		; \ set y pos of smoke
		STA $17C4,y		; /

		LDA $E4,x		; \ set x pos of smoke
		STA $17C8,y		; /

		LDA #$18		; \ set smoke duration
		STA $17CC,y		; /
		RTS

Graphics:	JSL !GetDrawInfo
		LDA $00
		STA $0300,y
		LDA $01
		STA $0301,y
		LDA #!Tile
		STA $0302,y

		LDA $157C,x
		CLC
		ROR A
		ROR A
		ROR A
		ORA $15F6,x		; Set the ccct portion
		ORA $64			; add level priority
		STA $0303,y

		LDY #$02		;16x16
		LDA #$00		;One tile
		JSL $01B7B3
		RTS

Offscreenhandle:	STZ $03
			PHB
			LDA #$01
			PHA
			PLB
			PEA $801F
			JML $81AC33

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Process interaction with other sprites
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Interact:	
	LDY.b #$0B		
LoopStart:
	CPY $15E9	;\
	BEQ NextSprite	;/dont kill self for god's sake.
	LDA $14C8,Y	; Skip dead sprites
	CMP #$08
	BCC NextSprite
	LDA $1686,Y		; Skip sprites that don't interact with others
	AND #$08
	BNE NextSprite
	JSR SprSprInteract
NextSprite:	
        DEY
	BPL LoopStart
	RTS

SprSprInteract:
	JSL $03B69F
	PHX
	TYX
	JSL $03B6E5
	PLX
	JSL $03B72B
	BCC Return1
	PHX
	TYX
        LDA #$02                ; Sprite status = star killed
        STA $14C8,x
	LDA #$D0
	STA $AA,x
	JSL $01AB72
	PLX

	LDA #$03		; Play sound and give 200 points
	STA $1DF9
	LDA #$01
	JSL $02ACE5
Return1:
	RTS

