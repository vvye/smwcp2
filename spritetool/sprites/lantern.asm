;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Lantern Sprite
;;	 by
;;  wiiqwertyuiop
;;
;; Uses extra bit?: Yes
;;
;; If the extra bit is set it wont go out
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Defines:
;;
incsrc subroutinedefs_xkas.asm

!Brightness = $07	; This is the starting brightness
!Sound = $17		;\ Sound to play
!Bank = $1DFC		;/
!AmountADD = $02	; amount to increase screen brightness by
!AmountDEC = $02	; amount to decrease screen brightness by
!Time = $FF		; If the extra bit is set, put this to $FF
!TileOne = $AC		; This is the tile to draw when not lit
!TileTwo = $AE		; Tile to draw when lit
!Xpos = $0F		; How close the fireball sprite must be for it to lite (X pos)
!Ypos = $08		; How close the fireball sprite must be for it to lite (Y pos)

;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INIT
;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
STZ $C2,x		; Clear sprite table
LDA #!TileOne		;\ Draw not lit tile
STA $1528,x		;/
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Main
;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB                     
PHK                     
PLB                     
JSR LightMyFire  
PLB   
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite code below
;;;;;;;;;;;;;;;;;;;;;;;;;;;

Restore:
LDA #$0F		;\ This is to avoid mess ups
STA $0DAE		;/
RTS

LightMyFire:
JSR Graphics		; Draw sprite GFX

LDA $71			;\
CMP #$09		; | Is mario dead? If so restore brightness
BEQ Restore		;/

; Dont really care

LDA $0100
CMP #$14
BNE SkipSkip
LDA $1510,x
BNE SkipSkip
LDA #!Brightness		
STA $0DAE
STA $1510,x
SkipSkip:


LDA $C2,x		;\
BNE ItBeTime		; | move on if flags set
LDA $1540,x		; |
BNE ItBeTime		;/

LDA #!TileOne		;\ Draw not lit tile
STA $1528,x		;/

LDY #$0A		;\
Loop:			; |
LDA $170B,y		; | Set up loop to check for mario fireballs, and Yoshi fireballs
CMP #$05		; |
BEQ AreWeHit		; |
CMP #$11		; |
BEQ AreWeHit		; |
BackToLoop:		; |
DEY			; |
BPL Loop		;/
RTS



AreWeHit:
LDA $E4,x		;\
SEC			; |
SBC $171F,y		; |
CMP #!Xpos		; | Check if we have the same X position
BCS BackToLoop		;/ If not go back to the loop


LDA $D8,x		;\
SEC			; |
SBC $1715,y		; |
CMP #!Ypos		; | Check to make sure we have the same Y position
BCS BackToLoop		;/  If not go back to the loop

LDA $170B,y		;\
CMP #$11		; | Let yoshi's fireballs go through
BEQ GoOnAheadz		;/

LDA #$00		;\ Kill fireball
STA $170B,y		;/

GoOnAheadz:
LDA $0DAE		;\
CLC			; | Increase brightness
ADC #!AmountADD		; |
STA $0DAE		;/

LDA #!Sound		;\ Play sound
STA !Bank		;/

LDA #!Time		;\ Set timer/flag
STA $1540,x		;/

LDA #!TileTwo		;\ Draw lit tile
STA $1528,x		;/
RTS

ItBeTime:
LDA $1540,x		;\ Is the timer zero?
BEQ Darker		;/ If so bracnh
LDA #$01		;\ Set flag
STA $C2,x		;/
BitSet:
LDA #!TileTwo		;\ Draw lit tile
STA $1528,x		;/
RTS

Darker:
LDA $7FAB10,x		;\
AND #$04		; | Is the extra bit set? If so return
BNE BitSet		;/
STZ $C2,x		; Clear flag
LDA $0DAE		;\
SEC			; | Make dark
SBC #!AmountDEC		; |
STA $0DAE		;/
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;
;; Draw sprite GFX
;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:
	JSL !GetDrawInfo 	; Before actually coding the graphics, we need a routine that will get the current sprite's value 
				; into the OAM.
				; The OAM being a place where the tile data for the current sprite will be stored.
	LDA $00			;\
	STA $0300,y		;/ Draw the X position of the sprite

	LDA $01			;\
	STA $0301,y		;/ Draw the Y position of the sprite

;Those 2 above are always needed to figure out the X/Y position of the sprite

	LDA $1528,x		;\ Tile to draw. 
	STA $0302,y		;/ 
				

	LDX $15E9
	LDA $15F6,x
	ORA $64
	STA $0303,y		; Write the YXPPCCCT property byte of the sprite
				

	INY			;\
	INY			; | The OAM is 8x8, but our sprite is 16x16 ..
	INY			; | So increment it 4 times.
	INY			;/

	LDY #$02		; Y ends with the tile size .. 02 means it's 16x16
	LDA #$00		; A -> number of tiles drawn - 1.
				; I drew only 1 tile so I put in 1-1 = 00.

	JSL $01B7B3		; Call the routine that draws the sprite.
	RTS			
