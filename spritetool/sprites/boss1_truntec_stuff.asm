incsrc "subroutinedefs_xkas.asm"

; values of C2
; 00	wires
; 01	root
; 02	spiky thing


; const
!leaves_props		= $29
!wires_props		= $0F		; put priority here maybe
!wires_bottom_props	= $2F

!wires_clp_x_disp	= $00		; clipping
!wires_clp_y_disp	= $00
!wires_clp_width	= $10
!wires_clp_height	= $70		; -4ish?

; ram
!x_pos_lo		= $E4
!x_pos_hi		= $14E0
!y_pos_lo		= $D8
!y_pos_hi		= $14D4
!x_speed		= $B6
!y_speed		= $AA
!spr_status		= $14C8

; misc
!spr_type		= $C2

; wires defines
!prewires_timer		= $163E
!wires_anim_index	= $1FD6
!wires_state		= $1504
!wires_timer		= $1540

; this is going to be the sprite for truntec's attacks

print "INIT ",pc
	; LDA !spr_type,x
	; BNE .not_wires
	LDY #$04		; get 0-3
	JSL !GetRand
	STA !wires_anim_index,x
; .not_wires
	RTL

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR main
	PLB
	RTL

main:
	LDA !spr_type,x
	ASL
	TAX
	REP #$20
	LDA .ptrs,x
	STA $00
	SEP #$20
	LDX $15E9
	JMP ($0000)
	
.ptrs
	dw wires_main
	dw root_main
	dw spiky_main
	
;-----------------------------------------------------------
; wires
	
{
wires_main:
	JSR wires_gfx
	LDA !spr_status,x
	EOR #$08
	ORA $9D
	BNE .return
	
	LDA !prewires_timer,x
	BNE .return
	
	LDA !wires_state,x
	CMP #$04
	BEQ .kill
	; AND #$03
	TAY
	LDA !wires_timer,x
	BEQ .next_state
	LDA .speed-1,y
	STA !y_speed,x
	JSL $81801A		; update y pos w/o gravity
	JMP wires_interaction
	
.next_state
	LDA .time-1,y
	STA !wires_timer,x
	INC !wires_state,x
.return
	RTS
	
.kill
	STZ !spr_status,x
	RTS
	
; go down, wait, go up
.time
	db $30,$20,$30	
.speed
	db $10,$00,$D0
	
wires_interaction:
	LDA !x_pos_lo,x
	CLC
	ADC #!wires_clp_x_disp
	STA $04
	LDA !x_pos_hi,x
	ADC #$00
	STA $0A
	
	LDA #!wires_clp_width
	STA $06

	LDA !y_pos_lo,x
	CLC
	ADC #!wires_clp_y_disp
	STA $05
	LDA !y_pos_hi,x
	ADC #$00
	STA $0B
	
	LDA #!wires_clp_height
	STA $07
	
	JSL $83B664		; get player clipping
	JSL $83B72B		; check for contact
	BCC .no_contact
	JSL $80F5B7		; hurt player
.no_contact
	RTS
	
wires_gfx:
	JSL !GetDrawInfo
	
	LDA !prewires_timer,x
	BEQ .wires
	
.leaves
	PHX
	LDA $14
	LSR
	LSR
	AND #$03
	TAX
	
	LDA $00
	CLC
	ADC ..x_disp,x
	STA $0300,y
	
	LDA $01
	CLC
	ADC ..y_disp,x
	; SEC			; pls
	; SBC #$10
	STA $0301,y
	
	LDA ..tilemap,x
	STA $0302,y
	
	LDA #!leaves_props
	ORA $64
	STA $0303,y
	
	PLX
	LDY #$00		; 8x8
	TYA			; 1 tile
	JSL $81B7B3
	RTS

..x_disp
	db $04-16,$FE-16,$06-16,$F9-16
	
..y_disp
	db $70,$7D,$86,$71
	
..tilemap
	db $2E,$2F,$2E,$2F

	
.wires
	LDA !wires_anim_index,x
	AND #$03
	PHX
	TAX
	LDA ..anim_offset,x
	STA $04
	
	LDA $14
	LSR
	LSR
	AND #$03
	STA $03
	
	LDX #$05
..loop
	LDA $00
	STA $0300,y
	
	LDA $01
	; SEC			; pls
	; SBC #$04
	CLC
	ADC ..y_disp,x
	STA $0301,y
	
	LDA $04
	CLC
	ADC $03
	PHX
	TAX
	LDA ..tilemap,x
	PLX
	STA $0302,y
	
	LDA #!wires_props
	ORA $64
	STA $0303,y
	
	INY
	INY
	INY
	INY
	DEX
	BPL ..loop
	
	LDA $03			; for the bottom tile
	AND #$01
	TAX
	
	LDA $00
	STA $0300,y
	
	LDA $01
	CLC
	ADC #$60		; 5C
	STA $0301,y
	
	LDA ..tilemap_bottom,x
	STA $0302,y
	
	LDA #!wires_bottom_props
	ORA $64
	STA $0303,y
	
	PLX
	LDY #$02		; 16x16
	LDA #$06		; 7 tiles
	JSL $81B7B3
	RTS
	
..y_disp
	; db $00-4,$10-4,$20-4,$30-4,$40-4,$50-4
	db $00,$10,$20,$30,$40,$50
	

..tilemap
	db $48,$68,$68,$48,$68,$48
	db $68,$48,$68,$68,$48,$68
	db $68,$48,$48,$68,$48,$48
	db $48,$68,$48,$48,$68,$68

..tilemap_bottom
	db $6A,$6C

..anim_offset
	db $00,$06,$0C,$12

}

;-----------------------------------------------------------
; root

{	
root_main:
	RTS

}
	
;-----------------------------------------------------------
; spiky thing
	
{
spiky_main:
	RTS
	
}

