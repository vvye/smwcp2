;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
print "INIT ",pc
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main bullet bill shooter code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Return:
RTS

Main:
PHX
LDX #$0B
.Loop0
LDA $14C8,x
CMP #$09
BNE +
LDA $9E,x
CMP #$53
BNE +
LDA $1540,x
CMP #$FE
BCC +
LDA #$FF
STA $1540,x
+
DEX
BPL .Loop0
PLX

LDA $17AB,x
BNE TimerOn
PHX
LDY #$01
LDX #$0B
.Loop
LDA $14C8,x
BEQ +
LDA $9E,x
CMP #$53
BNE +
LDY #$00
+
DEX
BPL .Loop
PLX

TYA
BEQ Return

LDA #$38
STA $17AB,x
BRA Return

TimerOn:
CMP #$01
BNE Return
STZ $17AB,x

;spawn it
LDA $178B,x
CMP $1C
LDA $1793,x
SBC $1D
BNE Return
LDA $179B,x
CMP $1A
LDA $17A3,x
SBC $1B
BNE Return
LDA $179B,x
SEC
SBC $1A
CLC
ADC #$10
CMP #$10
BCC Return
JSL $02A9DE
BMI Return

Generate:
LDA #$09
STA $1DFC
LDA #$53
STA $009E,y
LDA $179B,x
STA $00E4,y
LDA $17A3,x
STA $14E0,y
LDA $178B,x
STA $00D8,y
LDA $1793,x
STA $14D4,y
PHX
TYX
JSL $07F7D2
DEC $1540,x
PLX
LDA #$09
STA $14C8,y

LDA $00
PHA

LDA $1783,x
AND #$40
BEQ LEFT

LDA $178B,x
AND #$10
BNE SHOOT_UP2
LDA #$03
BRA STORE
SHOOT_UP2:
LDA #$03
BRA STORE

LEFT:
LDA $178B,x
AND #$10
BNE SHOOT_UP
LDA #$01 
BRA STORE
SHOOT_UP:
LDA #$01 
STORE:
STA $00C2,y
STA $00
JSR DrawSmoke
PLA
STA $00

RETURN:
RTS

;==================================================================
;Draw Smoke
;==================================================================

DrawSmoke:          LDY #$03                ; \ find a free slot to display effect
FINDFREE:           LDA $17C0,y             ;  |
                    BEQ FOUNDONE            ;  |
                    DEY                     ;  |
                    BPL FINDFREE            ;  |
                    RTS                     ; / return if no slots open

FOUNDONE:           LDA #$01                ; \ set effect graphic to smoke graphic
                    STA $17C0,y             ; /
                    LDA #$1B                ; \ set time to show smoke
                    STA $17CC,y             ; /
                    LDA $178B,x               ; \ smoke y position = generator y position
                    STA $17C4,y             ; /
                    LDA $179B,x               ; \ load generator x position and store it for later
                    STA $17C8,y             ; /
                    RTS